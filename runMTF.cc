#ifdef _WIN32
#define snprintf  _snprintf
#endif

#include "mtf/MTF.h"

#include "mtf/Tools/cvUtils.h"
#include "mtf/Tools/inputCV.h"
#include "mtf/Tools/parameters.h"
#include "mtf/Tools/datasets.h"
// classes for preprocessing the image
#include "mtf/Tools/PreProc.h"

#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif

#include <time.h>
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

namespace fs = boost::filesystem;

#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
using namespace mtf::params;

int main(int argc, char * argv[]) {
	// read general parameters
	int fargc =readParams("Tools/mtf.cfg");
	parseArgumentPairs(fargv, fargc);
	// read parameters specific to different modules
	fargc = readParams("Tools/modules.cfg");
	parseArgumentPairs(fargv, fargc);
	// parse command line arguments
	parseArgumentPairs(argv, argc, 1, 1);
	if(actor_id >= 0){
		actor = actors[actor_id];
		if(source_id >= 0){
			source_name = combined_sources[actor_id][source_id];
		}
	}
#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif
	cout << "*******************************\n";
	cout << "Using parameters:\n";
	cout << "n_trackers: " << n_trackers << "\n";
	cout << "actor_id: " << actor_id << "\n";
	cout << "source_id: " << source_id << "\n";
	cout << "source_name: " << source_name << "\n";
	cout << "actor: " << actor << "\n";
	cout << "steps_per_frame: " << steps_per_frame << "\n";
	cout << "pipeline: " << pipeline << "\n";
	cout << "img_source: " << img_source << "\n";
	cout << "show_cv_window: " << show_cv_window << "\n";
	cout << "read_objs: " << read_objs << "\n";
	cout << "record_frames: " << record_frames << "\n";
	cout << "patch_size: " << patch_size << "\n";
	cout << "read_obj_fname: " << read_obj_fname << "\n";
	cout << "read_obj_from_gt: " << read_obj_from_gt << "\n";
	cout << "show_warped_img: " << show_warped_img << "\n";
	cout << "pause_after_frame: " << pause_after_frame << "\n";
	cout << "write_tracking_data: " << write_tracking_data << "\n";
	cout << "mtf_sm: " << mtf_sm << "\n";
	cout << "mtf_am: " << mtf_am << "\n";
	cout << "mtf_ssm: " << mtf_ssm << "\n";

	cout << "*******************************\n";

	vector<mtf::TrackerBase*> trackers;
	InputBase *input_obj = nullptr;
	cv::VideoWriter output_obj;
	GaussianSmoothing *pre_proc = new GaussianSmoothing();

	cv::Point fps_origin(10, 20);
	double fps_font_size = 0.50;
	cv::Scalar fps_color(0, 255, 0);
	char fps_text[100];

	cv::Point err_origin(10, 40);
	double err_font_size = 0.50;
	cv::Scalar err_color(0, 255, 0);
	char err_text[100];

	if((img_source == SRC_USB_CAM) || (img_source == SRC_DIG_CAM)){
		source_name = nullptr;
		show_tracking_error = read_obj_from_gt = read_objs = 0;

	} else {
		source_path = new char[500];
		snprintf(source_path, 500, "%s/%s", root_path, actor);
	}

	if(source_path && !strcmp(source_path, "#")){
		source_path = nullptr;
	}
	if(source_fmt && !strcmp(source_fmt, "#")){
		source_fmt = nullptr;
	}

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input_obj = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input_obj = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif

	else {
		cout << "Invalid video pipeline provided\n";
		return 1;
	}
	if(!input_obj->init_success){
		printf("Pipeline could not be initialized successfully\n");
		return 0;
	}
	//printf("done initializing pipeline\n");

	cv::Mat cv_frame_rgb(input_obj->img_height, input_obj->img_width, CV_32FC3);
	cv::Mat cv_frame_gs(input_obj->img_height, input_obj->img_width, CV_32FC1);

	mtf::initImg(cv_frame_rgb, cv_frame_gs, input_obj->img_height, input_obj->img_width);

	source_name = input_obj->dev_name;
	source_fmt = input_obj->dev_fmt;
	source_path = input_obj->dev_path;
	int n_frames = input_obj->n_frames;

	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", n_frames);

	for(int i = 0; i < init_frame_id; i++){
		input_obj->updateFrame();
	}	
	bool init_obj_read = false;
	/*get objects to be tracked*/
	CVUtils util_obj;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		obj_struct* init_object = util_obj.readObjectFromGT(source_name, source_path, n_frames, init_frame_id, debug_mode);
		if(init_object){
			init_obj_read = true;
			for(int i = 0; i < n_trackers; i++) {
				init_objects.push_back(init_object);
			}
		} else{
			printf("Failed to read initial object from ground truth; using manual selection...\n");
		}

	}
	if(!init_obj_read && read_objs) {
		init_objects = util_obj.readObjectsFromFile(n_trackers, read_obj_fname, debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		//printf("calling getMultipleObjects with sel_quad_obj=%d\n", sel_quad_obj);
		init_objects = util_obj.getMultipleObjects(input_obj->getCVFrame(), n_trackers,
			patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname);
		//printf("done calling getMultipleObjects\n");
	}	

	//conv_corners = util_obj.ground_truth;

	mtf::updateImg<GaussianSmoothing>(cv_frame_rgb, cv_frame_gs, 
		input_obj->getCVFrame(), pre_proc);

	//ofstream eig_img_out;
	//ofstream cv_img_out;
	//eig_img_out.open("log/eig_img.txt", ios::out);
	//cv_img_out.open("log/cv_img.txt", ios::out);

	//printf("Using norm_pix_max: %f\n norm_pix_min: %f\n", norm_pix_max, norm_pix_min);
	

	/*********************************** initialize trackers ***********************************/
	if(n_trackers > 1){
		printf("Multi tracker setup enabled\n");
		write_tracking_data = 0;
	}
	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}
	mtf::ImgParams img_params(resx, resy, cv_frame_gs, grad_eps, hess_eps);
	FILE *multi_fid = nullptr;
	bool pre_proc_enabled = false;
	for(int i = 0; i < n_trackers; i++) {
		if(n_trackers > 1){
			multi_fid = readTrackerParams(multi_fid);
		}
		printf("Initializing tracker %d with object of size %f x %f\n", i,
			init_objects[i]->size_x, init_objects[i]->size_y);
		if(res_from_size){
			img_params.resx = init_objects[i]->size_x;
			img_params.resy = init_objects[i]->size_y;
		}		
		mtf::TrackerBase *new_tracker = mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm, &img_params);
		//delete(new_tracker);
		//new_tracker = nullptr;
		//return 0;
		if(!new_tracker){
			printf("Tracker could not be initialized successfully\n");
			exit(0);
		}
		if(new_tracker->rgbInput()){
			new_tracker->initialize(input_obj->getCVFrame(), init_objects[i]->corners);
		} else{
			new_tracker->initialize(init_objects[i]->corners);
		}
		if(!new_tracker->rgbInput()){ pre_proc_enabled = true; }
		trackers.push_back(new_tracker);
	}
	char cv_win_name[100];
	snprintf(cv_win_name, 100, "OpenCV Window :: %s", source_name);
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
	}
	
	//eig_img_out.close();
	//cv_img_out.close();
	//vector<double> fps_vector;
	//vector<double> fps_win_vector;
	//double *fps_array=new double[n_frames-1];

	show_tracking_error = show_tracking_error && read_obj_from_gt;
	reinit_from_gt = reinit_from_gt && read_obj_from_gt;

	if(show_tracking_error || reinit_from_gt){
		int valid_gt_frames = util_obj.ground_truth.size();
		if(valid_gt_frames < n_frames){
			if(valid_gt_frames>0){
				printf("Disabling tracking error computation since ground truth is only available for %d out of %d frames\n",
					valid_gt_frames, n_frames);
			}else{
				printf("Disabling tracking error computation since ground truth is not available\n");
			}				
			show_tracking_error = 0;
			reinit_from_gt = 0;
		}
	}

	if(record_frames){
		output_obj.open("Tracked_video.avi", CV_FOURCC('M', 'J', 'P', 'G'), 24, input_obj->getCVFrame().size());
	}
	FILE *tracking_data_fid = nullptr;
	if(write_tracking_data){		
		char tracking_data_dir[500];
		char tracking_data_path[500];

		snprintf(tracking_data_dir, 500, "log/tracking_data/%s/%s", actor, source_name);

		if(!fs::exists(tracking_data_dir)){
			printf("Tracking data directory: %s does not exist. Creating it...\n", tracking_data_dir);
			fs::create_directories(tracking_data_dir);
		}
		if(!tracking_data_fname){
			tracking_data_fname = new char[500];
			snprintf(tracking_data_fname, 500, "%s_%s_%s_%d", mtf_sm, mtf_am, mtf_ssm, hom_normalized_init);
		}
		snprintf(tracking_data_path, 500, "%s/%s.txt", tracking_data_dir, tracking_data_fname);
		printf("Writing tracking data to: %s\n", tracking_data_path);
		tracking_data_fid = fopen(tracking_data_path, "w");
		fprintf(tracking_data_fid, "frame ulx uly urx ury lrx lry llx lly\n");
	}

	ofstream fout;

	double fps = 0, fps_win = 0;
	double avg_fps = 0, avg_fps_win = 0;
	double avg_err = 0;
	if(update_templ){
		printf("Template update is enabled\n");
	}
	

	int frame_id = init_frame_id + 1;
	double tracking_err = 0;
	/*********************************** update trackers ***********************************/
	while(true) {
		if(show_tracking_error || write_tracking_data || reinit_from_gt){
			//printf("computing error for frame: %d\n", frame_id - 1);			
			tracking_err = 0;
			for(int corner_id = 0; corner_id < 4; corner_id++){
				double x_diff = util_obj.ground_truth[frame_id - 1].at<double>(0, corner_id) - trackers[0]->cv_corners[corner_id].x;
				double y_diff = util_obj.ground_truth[frame_id - 1].at<double>(1, corner_id) - trackers[0]->cv_corners[corner_id].y;
				//printf("x_diff: %f\n", x_diff);
				//printf("y_diff: %f\n", y_diff);
				tracking_err += sqrt((x_diff*x_diff) + (y_diff*y_diff));
			}
			tracking_err /= 4.0;
			//printf("tracking_err: %f\n", tracking_err);
			//printf("done computing error\n");
			if(!std::isinf(fps)){
				avg_err += (tracking_err - avg_err) / frame_id;
				//fps_vector.push_back(fps);	
			}
		}
		if(record_frames || show_cv_window) {
			/* draw tracker positions to OpenCV window */
			for(int i = 0; i < n_trackers; i++) {
				int col_id = i % util_obj.no_of_cols;
				line(input_obj->getCVFrameMutable(), trackers[i]->cv_corners[0], trackers[i]->cv_corners[1], util_obj.obj_cols[col_id], line_thickness);
				line(input_obj->getCVFrameMutable(), trackers[i]->cv_corners[1], trackers[i]->cv_corners[2], util_obj.obj_cols[col_id], line_thickness);
				line(input_obj->getCVFrameMutable(), trackers[i]->cv_corners[2], trackers[i]->cv_corners[3], util_obj.obj_cols[col_id], line_thickness);
				line(input_obj->getCVFrameMutable(), trackers[i]->cv_corners[3], trackers[i]->cv_corners[0], util_obj.obj_cols[col_id], line_thickness);
				putText(input_obj->getCVFrameMutable(), trackers[i]->name, trackers[i]->cv_corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, util_obj.obj_cols[col_id]);
			}
			snprintf(fps_text, 100, "frame: %d c: %12.6f a: %12.6f cw: %12.6f aw: %12.6f fps", frame_id, fps, avg_fps, fps_win, avg_fps_win);
			putText(input_obj->getCVFrameMutable(), fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

			if(show_tracking_error){
				snprintf(err_text, 100, "ce: %12.8f ae: %12.8f", tracking_err, avg_err);
				putText(input_obj->getCVFrameMutable(), err_text, err_origin, cv::FONT_HERSHEY_SIMPLEX, err_font_size, err_color);
			}

			if(record_frames){
				output_obj.write(input_obj->getCVFrame());
			}
			if(show_cv_window){
				imshow(cv_win_name, input_obj->getCVFrame());
				int pressed_key = cv::waitKey(1 - pause_after_frame);
				if(pressed_key == 27){
					break;
				} else if(pressed_key == 32){
					pause_after_frame = 1 - pause_after_frame;
				}
			}
		}
		if(!show_cv_window && frame_id % 50 == 0){
			printf("frame_id: %5d avg_fps: %15.9f avg_fps_win: %15.9f avg_err: %15.9f\n",
				frame_id, avg_fps, avg_fps_win, avg_err);
		}
		if(write_tracking_data){
			fprintf(tracking_data_fid, "frame%05d.jpg %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f\n", frame_id,
				trackers[0]->cv_corners[0].x, trackers[0]->cv_corners[0].y,
				trackers[0]->cv_corners[1].x, trackers[0]->cv_corners[1].y,
				trackers[0]->cv_corners[2].x, trackers[0]->cv_corners[2].y,
				trackers[0]->cv_corners[3].x, trackers[0]->cv_corners[3].y);
		}

		clock_t start_time_with_input = clock();
		// update frame
		input_obj->updateFrame();

		if(pre_proc_enabled){
			mtf::updateImg<GaussianSmoothing>(cv_frame_rgb, cv_frame_gs,
				input_obj->getCVFrame(), pre_proc);
		}
		clock_t start_time = clock();
		//update trackers		
		for(int i = 0; i < n_trackers; i++) {
			if(trackers[i]->rgbInput()){
				trackers[i]->update(input_obj->getCVFrame());
			} else{
				trackers[i]->update();
				if(update_templ){
					trackers[i]->initialize(trackers[i]->getRegion());
				}
				if(reinit_from_gt && tracking_err > err_thresh){
					printf("Tracker failure detected. Reinitializing...\n");
					trackers[i]->initialize(util_obj.ground_truth[frame_id]);
				}
			}
		}

		clock_t end_time = clock();
		fps = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time);
		fps_win = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time_with_input);

		if(!std::isinf(fps)){
			avg_fps += (fps - avg_fps) / frame_id;
		}
		if(!std::isinf(fps_win)){
			avg_fps_win += (fps_win - avg_fps_win) / frame_id;
		}
		if(n_frames>0 && ++frame_id >= n_frames){
			printf("==========End of input stream reached==========\n");
			break;
		}
	}

	//output_obj->close();
	//double avg_fps = accumulate( fps_vector.begin(), fps_vector.end(), 0.0 )/ fps_vector.size();
	//double avg_fps_win = accumulate( fps_win_vector.begin(), fps_win_vector.end(), 0.0 )/ fps_win_vector.size();

	printf("avg_fps: %15.10f\n", avg_fps);
	printf("avg_fps_with_input: %15.10f\n", avg_fps_win);
	if(show_tracking_error){
		printf("Average Tracking Error: %15.10f\n", avg_err);
	}
	if(write_tracking_data){
		fprintf(tracking_data_fid, "frame%05d.jpg %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f\n", frame_id,
			trackers[0]->cv_corners[0].x, trackers[0]->cv_corners[0].y,
			trackers[0]->cv_corners[1].x, trackers[0]->cv_corners[1].y,
			trackers[0]->cv_corners[2].x, trackers[0]->cv_corners[2].y,
			trackers[0]->cv_corners[3].x, trackers[0]->cv_corners[3].y);
		fclose(tracking_data_fid);
		FILE *tracking_stats_fid = fopen("log/tracking_stats.txt", "a");
		fprintf(tracking_stats_fid, "%s\t %s\t %s\t %s\t %d\t %s\t %15.9f\t %15.9f\t %15.9f\n",
			source_name, mtf_sm, mtf_am, mtf_ssm, hom_normalized_init, tracking_data_fname, avg_fps, avg_fps_win, avg_err);
		fclose(tracking_stats_fid);
	}
	if(record_frames){
		output_obj.release();
	}
	//for(int i = 0; i < n_trackers; i++) {
	//	delete(trackers[i]);
	//}
	//delete(input_obj);
	//delete(util_obj);

	//fout.open("log/log.txt", ios::app);
	//fout<<n_trackers<<"\t"<<tracker_ids<<"\t"<<steps_per_frame<<"\t"<<source_name<<"\t"<<patch_size<<"\t"<<avg_fps<<"\n";
	//fout.close();	

	//for(int i=0;i<n_trackers;i++){
	//trackers[i]->printSSDOfRegion(i);
	//trackers[i]->printErrorLog(i);
	//trackers[i]->printSSSDLog(i);
	//}
	return 0;
}
