cmake_minimum_required(VERSION 2.8)
include_directories(imacq
    mftracker
    tld
    3rdparty/cvblobs
    ${OpenCV_INCLUDE_DIRS})
link_directories(${OpenCV_LIB_DIR} 
3rdparty/cvblobs)

add_library(libopentld SHARED
    imacq/ImAcq.cpp
    mftracker/BB.cpp
    mftracker/BBPredict.cpp
    mftracker/FBTrack.cpp
    mftracker/Lk.cpp
    mftracker/Median.cpp
    Clustering.cpp
    DetectionResult.cpp
    DetectorCascade.cpp
    EnsembleClassifier.cpp
    ForegroundDetector.cpp
    MedianFlowTracker.cpp
    NNClassifier.cpp
    TLD.cpp
    TLDUtil.cpp
    VarianceFilter.cpp
    imacq/ImAcq.h
    mftracker/BB.h
    mftracker/BBPredict.h
    mftracker/FBTrack.h
    mftracker/Lk.h
    mftracker/Median.h
    Clustering.h
    DetectionResult.h
    DetectorCascade.h
    EnsembleClassifier.h
    ForegroundDetector.h
    IntegralImage.h
    MedianFlowTracker.h
    NNClassifier.h
    NormalizedPatch.h
    TLD.h
    TLDUtil.h
    VarianceFilter.h)

target_link_libraries(libopentld ${OpenCV_LIBS} cvblobs)

set_target_properties(libopentld PROPERTIES OUTPUT_NAME opentld)

