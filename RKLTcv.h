#ifndef RKLTcv_H
#define RKLTcv_H

#include "SearchMethod.h"
#include "opencv2/video/tracking.hpp"

#define RKLTcv_ENABLE_SPI true
#define RKLTcv_ENABLE_FEEDBACK true
#define RKLTcv_FAILURE_DETECTION true
#define RKLTcv_FAILURE_THRESH 15.0
#define RKLTcv_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct RKLTcvParams{
	bool enable_spi;
	bool enable_feedback;
	bool failure_detection;
	double failure_thresh;
	bool debug_mode;

	RKLTcvParams(bool _enable_spi, bool _enable_feedback,
		bool _failure_detection, double _failure_thresh,
		bool _debug_mode){
		enable_spi = _enable_spi;
		enable_feedback = _enable_feedback;
		failure_detection = _failure_detection;
		failure_thresh = _failure_thresh;
		debug_mode = _debug_mode;
	}
	RKLTcvParams(RKLTcvParams *params = nullptr) :
		enable_spi(RKLTcv_ENABLE_SPI),
		enable_feedback(RKLTcv_ENABLE_FEEDBACK),
		failure_detection(RKLTcv_FAILURE_DETECTION),
		failure_thresh(RKLTcv_FAILURE_THRESH),
		debug_mode(RKLTcv_DEBUG_MODE){
		if(params){
			enable_spi = params->enable_spi;
			enable_feedback = params->enable_feedback;
			failure_detection = params->failure_detection;
			failure_thresh = params->failure_thresh;
			debug_mode = params->debug_mode;
		}
	}
};

template<class AM, class SSM>
class RKLTcv : public TrackerBase {

public:

	typedef SearchMethod < AM, SSM > TemplTrackerType;

	typedef RKLTcvParams ParamType;
	ParamType params;

	TemplTrackerType *templ_tracker;
	cv::Mat prev_img, curr_img;

	cv::Mat grid_corners_mat;


	RKLTcv(const cv::Mat &img,
		ParamType *rklt_params,
		TemplTrackerType *_templ_tracker) :
		TrackerBase(), params(rklt_params),
		templ_tracker(_templ_tracker){
		printf("\n");
		printf("Initializing OpenCV RKL tracker with:\n");
		printf("enable_spi: %d\n", params.enable_spi);
		printf("enable_feedback: %d\n", params.enable_feedback);
		printf("failure_detection: %d\n", params.failure_detection);
		printf("failure_thresh: %f\n", params.failure_thresh);

		printf("debug_mode: %d\n", params.debug_mode);
		printf("templ_tracker: %s with:\n", templ_tracker->name.c_str());
		printf("\t appearance model: %s\n", templ_tracker->am->name.c_str());
		printf("\t state space model: %s\n", templ_tracker->ssm->name.c_str());
		printf("\n");


		curr_img = img;
		if(params.enable_spi){
			if(!templ_tracker->supportsSPI()){
				printf("Template tracker does not support SPI so disabling it\n");
				params.enable_spi = false;
			} else{
				printf("SPI is enabled\n");
			}
		}
		if(params.failure_detection){
			printf("Template tracker failure detection is enabled with a threshold of %f\n",
				params.failure_thresh);
			grid_corners_mat.create(2, 4, CV_64FC1);
		}
	}

	void initialize(const cv::Mat &cv_img, const cv::Mat &corners) override {
		templ_tracker->initialize(cv_img, corners);

		cv_corners_mat = templ_tracker->getRegion();
		convertMatToPoint2D(cv_corners, cv_corners_mat);
	}
	void initialize(const cv::Mat &corners) override {
		curr_img.copyTo(prev_img);

		templ_tracker->initialize(corners);

		cv_corners_mat = templ_tracker->getRegion();
		convertMatToPoint2D(cv_corners, cv_corners_mat);
	}

	void update(const cv::Mat &cv_img) override {
		cv::calcOpticalFlowPyrLK(prev_img, curr_img,
			prevPts, nextPts, OutputArray status, OutputArray err,
			Size winSize = Size(21, 21),
			int maxLevel = 3, TermCriteria criteria = TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 30, 0.01),
			int flags = 0, double minEigThreshold = 1e-4);
		templ_tracker->setRegion();
		if(params.enable_spi){
			templ_tracker->setSPIMask((const bool*)grid_tracker->getPixMask());
		}
		templ_tracker->update(cv_img);
		postUpdateProc();
	}

	void update() override {
		templ_tracker->setRegion(grid_tracker->getRegion());
		if(params.enable_spi){
			templ_tracker->setSPIMask((const bool*)grid_tracker->getPixMask());
		}
		templ_tracker->update();
		postUpdateProc();
	}
	void postUpdateProc(){
		cv_corners_mat = templ_tracker->getRegion();
		if(params.failure_detection){
			grid_corners_mat = grid_tracker->getRegion();
			double corner_diff = cv::norm(cv_corners_mat, grid_corners_mat);
			if(corner_diff > params.failure_thresh){
				convertMatToPoint2D(cv_corners, grid_corners_mat);
				return;
			}
		}
		if(params.enable_feedback){
			grid_tracker->setRegion(cv_corners_mat);
		}
		convertMatToPoint2D(cv_corners, cv_corners_mat);
	}
private:
	~RKLTcv(){}
	inline void convertMatToPoint2D(cv::Point2d *corners,
		const cv::Mat &corners_mat){
		for(int i = 0; i < 4; i++){
			corners[i].x = corners_mat.at<double>(0, i);
			corners[i].y = corners_mat.at<double>(1, i);
		}
	}
};
_MTF_END_NAMESPACE

#endif

