#include "memwatch.h"
#include "search_graph_knn.h"

namespace gnn{
	//------------------------------
	struct indx_dist *intersect(struct indx_dist *gnns, struct indx_dist *tnns, int K, int *knns_size)
	{
		struct indx_dist *knns = static_cast<indx_dist*>(malloc(K*sizeof(struct indx_dist)));
		check_pointer(knns, "Couldn't malloc knns");
		int found, s = 0;
		for(int i = 0; i < K; i++)
		{
			found = 0;
			for(int j = 0; j < K; j++)
				if(gnns[i].idx == tnns[j].idx)
				{
					found = 1;  break;
				}
			if(found)
			{
				knns[s].idx = gnns[i].idx;
				knns[s++].dist = gnns[i].dist;
			}
		}
		// s might be 0, no realloc to 0
		//knns = realloc(knns, s*sizeof(struct indx_dist)); //no need anymore
		//check_pointer(knns, "Couldn't realloc knns"); // no need anymore
		*knns_size = s;

		return knns;
	}

	//-------------------------------
	void pick_knns(struct indx_dist *vis_nodes, int visited, struct indx_dist **gnn_dists, int K, int *gnns_cap)
	{
		qsort(vis_nodes, visited, sizeof(vis_nodes[0]), cmp_qsort); //Ascending...
		if(K > *gnns_cap)
		{
			*gnns_cap = K;
			*gnn_dists = static_cast<indx_dist*>(realloc(*gnn_dists, K*sizeof(struct indx_dist))); // not needed? not sure
			check_pointer(*gnn_dists, "Couldn't realloc *gnns_dists");
		}
		int found = 0, ii = 0, jj = 0;
		(*gnn_dists)[jj].idx = vis_nodes[ii].idx;
		(*gnn_dists)[jj].dist = vis_nodes[ii].dist;
		ii++; jj++;

		while(jj < K)
		{
			//  i++;
			found = 0;
			for(int j = 0; j < jj; j++)
				if((*gnn_dists)[j].idx == vis_nodes[ii].idx)
				{
					found = 1; break;
				}

			if(found)
			{
				ii++; continue;
			} else
			{
				(*gnn_dists)[jj].idx = vis_nodes[ii].idx;
				(*gnn_dists)[jj].dist = vis_nodes[ii].dist;
				jj++; ii++;
			}
		}
	}

	//--------------------------------
	int *sample_X(struct node *Nodes, int node_ind, int n_dims, int *X)
	{
		int *X1 = static_cast<int*>(malloc(Nodes[node_ind].size * n_dims * sizeof(int)));
		for(int i = 0; i < Nodes[node_ind].size; i++)
			for(int j = 0; j < n_dims; j++)
				X1[i*n_dims + j] = X[Nodes[node_ind].nns_inds[i] * n_dims + j];

		return X1;
	}
}


