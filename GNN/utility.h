
#ifndef UTILITY_H
#define UTILITY_H

//#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //<string>
#include <math.h>
//#include <array.h>
#include <time.h>
#include <sys/time.h>
//#include <iostream>
//#include <algorithm>

#define db_17k  17056
#define db_50k  50000
#define db_118k 118607
#define db_204k 204727 

#define LINE_LEN 1000

/*
struct node{
  int *nns_inds;
  //double *nns_dists;
  int size;
  int capacity;
};
*/
namespace gnn{
	struct indx_dist{
		double dist;
		int idx;
	};

	struct mat_size{
		int rows;
		int cols;
	};

	//bool cmp_cppsort(struct indx_dist a, struct indx_dist b);
	int my_rand(int lb, int ub);
	int cmp_qsort(const void *a, const void *b);
	int found(int *list, int ind, int list_size);
	void check_pointer(void *ptr, char *msg);
	double my_dist(int *v1, int *v2, int length);
	void mergesort(struct indx_dist arr[], int low, int high);
	void merge(struct indx_dist arr[], int low, int split, int high);
	void swap_int(int *i, int *j);
	void swap_double(double *i, double *j);

	//--------------------------------
	template<typename DistType>
	void knn_search2(double *Q, struct indx_dist *dists, double *X, int rows, int cols, int k,
		const DistType &dist_func)
	{
		// Faster version of knn_search
		// Calculates the distance of query to all data points in X and returns the sorted dist array
		/*  for (i=0; i<rows; i++)
		{
		dists[i].dist = dist_func(Q, X+i*cols, cols);
		dists[i].idx = i;
		}
		mergesort(dists, 0, rows-1);
		*/
		int index, ii, count = 0;
		//int capacity = k;
		for(index = 0; index < rows; index++)
		{
			double *point = X + index*cols;

			for(ii = 0; ii < count; ++ii) {
				if(dists[ii].idx == ii) continue; //return false;
			}
			//addPoint(point);
			double dist = dist_func(Q, point, cols);
			if(count < k)
			{
				dists[count].idx = index;
				dists[count].dist = dist;
				++count;
			} else if(dist < dists[count - 1].dist || (dist == dists[count - 1].dist &&
				index < dists[count - 1].idx)) {
				//         else if (dist < dists[count-1]) {
				dists[count - 1].idx = index;
				dists[count - 1].dist = dist;
			} else {
				continue;   //  return false;
			}

			int i = count - 1;
			while(i >= 1 && (dists[i].dist < dists[i - 1].dist || (dists[i].dist == dists[i - 1].dist &&
				dists[i].idx < dists[i - 1].idx)))
			{
				swap_int(&dists[i].idx, &dists[i - 1].idx);
				swap_double(&dists[i].dist, &dists[i - 1].dist);
				i--;
			}

			//return false;
		}
	}

	//--------------------------------
	template<typename DistType>
	void knn_search11(double *Q, struct indx_dist *dists, double *X, int rows, int cols, int k, int *X_inds,
		const DistType &dist_func)
	{
		// Faster version of knn_search1
		// Calculates the distance of query to all data points in X and returns the sorted dist array

		int count = 0;
		for(int i = 0; i < rows; i++)
		{

			for(int j = 0; j < count; ++j) {
				if(dists[j].idx == j) continue; //return false;
			}
			double *point = X + X_inds[i] * cols;
			double dist = dist_func(Q, point, cols);
			if(count < k)
			{
				dists[count].idx = i;
				dists[count].dist = dist;
				++count;
			} else if(dist < dists[count - 1].dist || (dist == dists[count - 1].dist &&
				i < dists[count - 1].idx)) {
				//         else if (dist < dists[count-1]) {
				dists[count - 1].idx = i;
				dists[count - 1].dist = dist;
			} else {
				continue;      //  return false;
			}

			int ii = count - 1;
			while(ii >= 1 && (dists[ii].dist < dists[ii - 1].dist || (dists[ii].dist == dists[ii - 1].dist &&
				dists[ii].idx < dists[ii - 1].idx)))
			{
				swap_int(&dists[ii].idx, &dists[ii - 1].idx);
				swap_double(&dists[ii].dist, &dists[ii - 1].dist);
				ii--;
			}
		}
	}
}
#endif
