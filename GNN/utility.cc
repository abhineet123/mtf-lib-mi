
#include "memwatch.h"
#include "utility.h"

namespace gnn{
	//--------------------------------
	int found(int *inds, int ind, int num_inds)
	{
		for(int i = 0; i < num_inds; i++)
		{
			if(inds[i] == ind)
				return 1;
		}
		return 0;
	}

	//-------------------------------
	void swap_int(int *i, int *j)
	{
		int temp;
		temp = *i;
		*i = *j;
		*j = temp;
	}

	//-------------------------------
	void swap_double(double *i, double *j)
	{
		double temp;
		temp = *i;
		*i = *j;
		*j = temp;
	}


	//--------------------------------
	int cmp_qsort(const void *a, const void *b)
	{
		struct indx_dist *a1 = (struct indx_dist *)a;
		struct indx_dist *b1 = (struct indx_dist *)b;

		// Ascending
		if(a1->dist > b1->dist) return 1;
		else if(a1->dist == b1->dist) return 0;
		else return -1;
	}

	//--------------------------------
	/*inline bool cmp_cppsort(struct indx_dist a, struct indx_dist b)
	{
	// Ascending
	return a.dist < b.dist;
	}
	*/


	//-------------------------------
	void mergesort(struct indx_dist array[], int left, int right)
	{
		int splitpos;
		if(left < right)
		{
			splitpos = (left + right) / 2;
			mergesort(array, left, splitpos);
			mergesort(array, splitpos + 1, right);
			merge(array, left, splitpos, right);
		}
	}

	void merge(struct indx_dist array[], int left, int splitpos, int right)
	{
		int size_arr1 = splitpos - left + 1;
		int size_arr2 = right - splitpos;
		struct indx_dist arr1[size_arr1];
		struct indx_dist arr2[size_arr2];
		int k, i = 0, j = 0;

		for(i = 0; i < size_arr1; i++)
		{
			arr1[i].dist = array[left + i].dist;
			arr1[i].idx = array[left + i].idx;
		}

		for(j = 0; j < size_arr2; j++)
		{
			arr2[j].dist = array[splitpos + j + 1].dist;
			arr2[j].idx = array[splitpos + j + 1].idx;
		}
		i = 0; j = 0;
		for(k = left; k <= right; k++)
		{
			if(j == size_arr2)
			{
				array[k].dist = arr1[i].dist;
				array[k].idx = arr1[i].idx;
				i++;
			} else if(i == size_arr1)
			{
				array[k].dist = arr2[j].dist;
				array[k].idx = arr2[j].idx;
				j++;
			} else if(arr1[i].dist <= arr2[j].dist)
			{
				array[k].dist = arr1[i].dist;
				array[k].idx = arr1[i].idx;
				i++;
			} else
			{
				array[k].dist = arr2[j].dist;
				array[k].idx = arr2[j].idx;
				j++;
			}
		}
	}

	//--------------------------------
	void check_pointer(void *ptr, char *msg)
	{
		if(ptr == NULL)
		{
			printf("%s\n", msg);
			exit(EXIT_FAILURE);
		}
	}

	//--------------------------------
	int my_rand(int lb, int ub)
	{
		//  time_t sec;
		//  time(&sec);
		//  srand((unsigned int) sec);
		return (rand() % (ub - lb + 1) + lb);
	}

	//--------------------------------
	double my_dist(int *v1, int *v2, int vlen)
	{
		double sum = 0;
		for(int i = 0; i < vlen; i++)
			sum = sum + (v1[i] - v2[i])*(v1[i] - v2[i]);//pow(v1[i]-v2[i], 2);
		return sum;//sqrt(sum); 
	}
}









