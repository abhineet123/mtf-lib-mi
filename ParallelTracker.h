#ifndef PARALLEL_TRACKER_H
#define PARALLEL_TRACKER_H

#include "SearchMethod.h"
#include "miscUtils.h"

#ifdef ENABLE_TBB
#include "tbb/tbb.h" 
#endif

#define PARL_ESTIMATION_METHOD 0
#define PARL_RESET_TO_MEAN 0


_MTF_BEGIN_NAMESPACE

struct ParallelParams {
	enum class EstimationMethod {
		MeanOfCorners,
		MeanOfState
	};
	EstimationMethod estimation_method;
	bool reset_to_mean;
	const char* operator()(EstimationMethod _estimation_method) const;
	ParallelParams(EstimationMethod _estimation_method, bool _reset_to_mean);
	ParallelParams(ParallelParams *params = nullptr);
};

template<class AM, class SSM>
class ParallelTracker : public SearchMethod<AM, SSM> {

public:

	typedef SearchMethod<AM, SSM> SM;
	using SM::name;
	using SM::cv_corners;
	using SM::cv_corners_mat;
	using SM::ssm;
	using typename SM::SSMParams;

	cv::Mat curr_img;
	bool curr_img_updated;

	typedef ParallelParams ParamType;
	ParamType params;

	typedef ParamType::EstimationMethod EstimationMethod;

	const vector<SM*> trackers;
	int n_trackers;

	cv::Mat mean_corners_cv;
	CornersT mean_corners;

	int ssm_state_size;
	std::vector<VectorXd> ssm_states;
	VectorXd mean_state;

	ParallelTracker(const vector<SM*> _trackers, ParamType *parl_params,
		int resx, int resy, SSMParams *ssm_params);
	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void update(const cv::Mat &img) override;
	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override ;
	void setRegion(const cv::Mat& corners)  override;
	// return true if the tracker requires the raw RGB image
	virtual bool rgbInput() const override{ return false; }
};

// Generalized version that takes TrackerBase trackers instead

class ParallelTracker2 : public TrackerBase {

public:
	typedef ParallelParams ParamType;
	ParamType params;

	cv::Mat curr_img;
	bool curr_img_updated;

	typedef ParamType::EstimationMethod EstimationMethod;

	const vector<TrackerBase*> trackers;
	int n_trackers;

	cv::Mat mean_corners_cv;

	ParallelTracker2(const vector<TrackerBase*> _trackers, ParamType *parl_params);
	void initialize(const cv::Mat &img, const cv::Mat &corners) override;
	void update(const cv::Mat &img) override;

	void initialize(const cv::Mat &corners) override;
	void update() override;
	const cv::Mat& getRegion() override {
		return mean_corners_cv;
	}

	void setRegion(const cv::Mat& corners)  override;
private:
	void convertMatToPoint2D(cv::Point2d *cv_corners,
		const cv::Mat &cv_corners_mat);
};

_MTF_END_NAMESPACE

#endif

