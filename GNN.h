#ifndef GNN_H
#define GNN_H

#include "SearchMethod.h"

#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>

#include "GNN/build_graph.h"

#include "miscUtils.h"

#define GNN_MAX_ITERS 10
#define GNN_K 250
#define GNN_UPD_THRESH 0.01
#define GNN_CORNER_SIGMA_D 0.06
#define GNN_CORNER_SIGMA_T 0.04
#define GNN_N_SAMPLES 1000
#define GNN_ADDITIVE_UPDATE 1
#define GNN_DIRECT_SAMPLES 1
#define GNN_DEBUG_MODE false
#define GNN_CORNER_GRAD_EPS 1e-5

_MTF_BEGIN_NAMESPACE

struct GNNParams{
	int max_iters; //! maximum iterations of the GNN algorithm to run for each frame
	int n_samples;
	int k;

	double upd_thresh; //! maximum L2 norm of the state update vector at which to stop the iterations	
	double corner_sigma_d;
	double corner_sigma_t;

	bool additive_update;
	bool direct_samples;


	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	GNNParams(int _max_iters, int _n_samples, int _k,
		double _upd_thresh,
		double _corner_sigma_d, double _corner_sigma_t,
		bool _additive_update, bool _direct_samples,
		bool _debug_mode){
		max_iters = _max_iters;
		n_samples = _n_samples;
		k = _k;
		upd_thresh = _upd_thresh;
		corner_sigma_d = _corner_sigma_d;
		corner_sigma_t = _corner_sigma_t;
		additive_update = _additive_update;
		direct_samples = _direct_samples;
		debug_mode = _debug_mode;
	}

	GNNParams(GNNParams *params = nullptr) :
		max_iters(GNN_MAX_ITERS),
		n_samples(GNN_N_SAMPLES),
		k(GNN_K),
		upd_thresh(GNN_UPD_THRESH),
		corner_sigma_d(GNN_CORNER_SIGMA_D),
		corner_sigma_t(GNN_CORNER_SIGMA_T),
		additive_update(GNN_ADDITIVE_UPDATE),
		direct_samples(GNN_DIRECT_SAMPLES),
		debug_mode(GNN_DEBUG_MODE){
		if(params){
			max_iters = params->max_iters;
			n_samples = params->n_samples;
			k = params->k;
			upd_thresh = params->upd_thresh;
			corner_sigma_d = params->corner_sigma_d;
			corner_sigma_t = params->corner_sigma_t;
			additive_update = params->additive_update;
			direct_samples = params->direct_samples;
			debug_mode = params->debug_mode;
		}
	}
};

//struct indx_dist{
//	double dist;
//	int idx;
//};
//
//struct node{
//	int *nns_inds;
//	int size;
//	int capacity;
//};


template<class AM, class SSM>
class GNN : public SearchMethod < AM, SSM > {
private:
	init_profiling();
	char *log_fname;
	char *time_fname;

public:
	//typedef typename AM::DistanceMeasure DistanceMeasure;
	//DistanceMeasure dist_obj;

	typedef boost::minstd_rand baseGeneratorType;
	typedef boost::normal_distribution<> dustributionType;
	typedef boost::variate_generator<baseGeneratorType&, dustributionType> randomGeneratorType;


	typedef GNNParams ParamType;
	ParamType params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::cv_corners;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	struct gnn::node *Nodes;

	int frame_id;
	int am_feat_size;
	int ssm_state_size;

	Matrix3d warp_update;

	Matrix24d prev_corners;
	VectorXd ssm_update;	
	VectorXd inv_state_update;
	VectorXd ssm_sigma;
	MatrixXd ssm_perturbations;

	MatrixXd eig_dataset;
	VectorXd eig_query;
	VectorXi eig_result;
	VectorXd eig_dists;

	//flannResultType *flann_result;
	//flannMatType *flann_dists;

	int best_idx;
	double best_dist;

	GNN(ParamType *nn_params,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	~GNN(){}

	void initialize(const cv::Mat &corners) override;
	void update() override;

	void swap_int(int *i, int *j){
		int temp;
		temp = *i;
		*i = *j;
		*j = temp;
	}
	void swap_double(double *i, double *j){
		double temp;
		temp = *i;
		*i = *j;
		*j = temp;
	}
};
_MTF_END_NAMESPACE

#endif

