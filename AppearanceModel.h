#ifndef APPEARANCE_MODEL_H
#define APPEARANCE_MODEL_H

#define am_not_implemeted(func_name) \
	stringstream excp_msg; \
	excp_msg<<name<<" :: "<< #func_name<< ":: Not implemented Yet"; \
	throw std::domain_error(excp_msg.str())

#include "ImageBase.h"
#include <stdexcept>


_MTF_BEGIN_NAMESPACE

// set of indicator variables to keep track of which dependent state variables need updating and executing
// the update expression for any variable only when at least one of the other variables it depends on has 
// been updated; this can help to increase speed by avoiding repeated computations
struct AMStatus : public PixStatus{
	bool similarity, grad, hess;

	AMStatus() : PixStatus(){ clear(); }

	void set(){
		setPixState();
		similarity = grad = hess = true;
	}
	void clear(){
		clearPixState();
		similarity = grad = hess = false;
	}
};

class AppearanceModel : public ImageBase{	

public:
	string name; //! name of the appearance model

	double similarity; //! measures the similarity between the current and initial patch
	//! this is the quantity to be maximized by the optimization process

	RowVectorXd init_grad, curr_grad; //! 1 x N gradients of the similarity
	//! w.r.t. initial and current pixel values; 

	// these NxN Hessians of the similarity wrt pixel values are not stored or computed explicitly because:
	// 1. the matrices are just too large for higher sampling resolutions
	// 2. they are often very sparse so allocating so much space is wasteful
	// 3. computing the Hessian wrt SSM parameters by multiplying this matrix with the SSM Hessian is highly inefficient is highly inefficient

	//MatrixXd init_hess, curr_hess;

	explicit AppearanceModel(ImgParams *img_params = nullptr) :
		ImageBase(img_params){
		first_iter = false;
		spi_mask = nullptr;
		similarity = 0;
	}

	virtual ~AppearanceModel(){}

	// accessor methods; these are not defined as 'const' since an appearance model may like to
	// make some last moment changes to the variable being accessed before returning it to can avoid any 
	// unnecessary computations concerning the variable (e.g. in its 'update' function) unless it is actually accessed;
	virtual double getSimilarity(){ return similarity; }
	// returns a normalized version of the similarity that lies between 0 and 1
	// and can be interpreted as the likelihood that the current patch represents 
	// the same object as the initial patch
	virtual double getLikelihood(){am_not_implemeted(getLikelihood);}
	virtual const RowVectorXd& getInitGrad(){ return init_grad; }
	virtual const RowVectorXd& getCurrGrad(){ return curr_grad; }


	// modifier methods;
	virtual void setSimilarity(double _similarity){ similarity = _similarity; }
	virtual void setInitGrad(const RowVectorXd &_init_grad){ init_grad = _init_grad; }
	virtual void setCurrGrad(const RowVectorXd &_curr_grad){ curr_grad = _curr_grad; }

	/*
	if any of the "initialize" functions are reimplemented, there should be a statement there copying the computed value in the "init"
	variable to the corresponding "curr" variable so the two have the same value after this function is called;

	Note for the SM: the "initialize" function for any state variable whose "update" function will be called later should be called
	once from the SM's own initialize function even if the initial value of that variable will not be used later;
	this is because updating the state variable may involve some computations which need to be performed only once and the AM is free to delegate
	any such computations to the respective "initialize" function to avoid repeating them in the "update" function which can have a negative impact on performance;
	thus if this function is not called, the results of these computations that are needed by the "update" function will remain uncomputed leading to undefined behavior;

	also the initialize functions have a boolean indicator parameter called "is_initialized" which defaults to true but should be set to false
	if they are called only to update the internal state to take into account any changes to the initial template, i.e. if they are called
	again after the first call to initialize the state (when it should be left to true)
	*/

	// if inverted_model is true, then the initial image is to be treated as current and vice versa while computing the differentials;
	// should make no difference to symmetric models but will matter for non-symmetric ones like CCRE;
	virtual void initialize(){	am_not_implemeted(initialize); }
	virtual void initializeGrad() { am_not_implemeted(initializeGrad); }
	// even though the Hessian of the error norm w.r.t. pixel values is not a state variable (since it does not need
	// to be computed separately to get the Hessian w.r.t SSM), this function is provided as a place to perform 
	// any one-time computations that may help to decrease the runtime cost of the interfacing function that computes this Hessian
	virtual void initializeHess(){ am_not_implemeted(initializeHess); }

	// -------- functions for updating state variables when a new image arrives -------- //

	// prereq_only should be left to true if update is only called to compute the prerequisites 
	// for the two gradient functions and the actual value of the appearance model is not needed
	virtual void update(bool prereq_only = true){ am_not_implemeted(update); }
	virtual void updateInitGrad(){ am_not_implemeted(updateInitGrad); }
	virtual void updateCurrGrad() { am_not_implemeted(updateCurrGrad); }
	//virtual void updateInitHess() { throw std::domain_error("updateInitHess :: Not implemented Yet"); }
	//virtual void updateCurrHess() { throw std::domain_error("updateCurrHess :: Not implemented Yet"); }

	// ----- interfacing functions that take pixel jacobians/Hessians w.r.t. SSM parameters and combine them with AM jacobians/Hessians w.r.t. pixel values ---- //

	//! to produce corresponding jacobians w.r.t. SSM parameters
	virtual void cmptInitJacobian(RowVectorXd &init_similarity_jacobian, const MatrixXd &init_pix_jacobian){
		assert(init_pix_jacobian.rows() == n_pix && init_pix_jacobian.cols() == init_similarity_jacobian.cols());
		init_similarity_jacobian.noalias() = init_grad * init_pix_jacobian;
	}
	virtual void cmptCurrJacobian(RowVectorXd &similarity_jacobian, const MatrixXd &curr_pix_jacobian){
		assert(curr_pix_jacobian.rows() == n_pix && curr_pix_jacobian.cols() == similarity_jacobian.cols());
		similarity_jacobian.noalias() = curr_grad * curr_pix_jacobian;
	}
	//! multiplies the gradients of the current error norm w.r.t. current and initial pixel values with the respective pixel jacobians and computes
	//! the difference between the resultant jacobians of the current error norm  w.r.t. external (e.g. SSM) parameters
	//! though this can be done by the search method itself, a dedicated method is provided to take advantage of any special constraints to
	//! speed up the computations; if mean of the two pixel jacobians is involved in this computation, it is stored in the provided matrix to 
	//! avoid recomputing it while computing the error norm hessian
	virtual void cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		diff_of_jacobians.noalias() = (curr_grad * curr_pix_jacobian) - (init_grad * init_pix_jacobian);
	}

	//! compute the S x S Hessian of the error norm using the supplied N x S Jacobian of pixel values w.r.t. external parameters
	//! compute the approximate Hessian be ignoring the second order terms
	virtual void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian){
		am_not_implemeted(cmptInitHessian(first order));
	}
	virtual void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian){
		am_not_implemeted(cmptCurrHessian(first order));
	}
	virtual void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian) {
		am_not_implemeted(cmptSelfHessian(first order));
	}

	//! compute the exact Hessian by considering the second order terms too
	virtual void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian, const MatrixXd &init_pix_hessian){
		am_not_implemeted(cmptInitHessian(second order));
	}
	virtual void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian){
		am_not_implemeted(cmptCurrHessian(second order));
	}
	virtual void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) {
		am_not_implemeted(cmptSelfHessian (second order));
	}

	//! analogous to cmptDifferenceOfJacobians except for computing the mean of the current and initial Hessians
	virtual void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		int ssm_state_size = curr_pix_jacobian.cols();
		assert(sum_of_hessians.rows() == ssm_state_size && sum_of_hessians.cols() == ssm_state_size);
		MatrixXd init_hessian(ssm_state_size, ssm_state_size);
		cmptInitHessian(init_hessian, init_pix_jacobian);

		MatrixXd curr_hessian(ssm_state_size, ssm_state_size);
		cmptCurrHessian(curr_hessian, curr_pix_jacobian);

		sum_of_hessians = init_hessian + curr_hessian;
	}
	virtual void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &init_pix_hessian, const MatrixXd &curr_pix_hessian){
		int ssm_state_size = curr_pix_jacobian.cols();
		assert(sum_of_hessians.rows() == ssm_state_size && sum_of_hessians.cols() == ssm_state_size);
		MatrixXd init_hessian(ssm_state_size, ssm_state_size);
		cmptInitHessian(init_hessian, init_pix_jacobian, init_pix_hessian);

		MatrixXd curr_hessian(ssm_state_size, ssm_state_size);
		cmptCurrHessian(curr_hessian, curr_pix_jacobian, curr_pix_hessian);

		sum_of_hessians = init_hessian + curr_hessian;
	}

	// pixels corresponding to false entries in the mask will be ignored in all respective computations where pixel values are used;
	//it is up to the AM to do this in a way that makes sense; since none of the state variables are actually being resized they will 
	// still have entries corresponding to these ignored pixels but the AM s at liberty to put anything there assuming
	// that the SM will not use these entries in its own computations; this is why all of these have default implementations that simply ignore the mask;
	// these can be used by the AM when the non masked entries of the computed variable do not depend on the masked pixels; 
	const bool *spi_mask;
	virtual void setSPIMask(const bool *_spi_mask){ spi_mask = _spi_mask; }
	virtual void clearSPIMask(){ spi_mask = nullptr; }
	virtual bool supportsSPI(){ return false; }// should be overridden by an implementing class once 
	// it implements SPI functionality for all functions where it makes logical sense

	// indicator variable that can be set by iterative search methods to indicate if the initial or first iteration is being run on the current image;
	// can be used to perform some costly operations/updates only once per frame rather than at every iteration
	bool first_iter;
	// should be called before performing the first iteration on a new image to indicate that the image
	// has changed since the last time the update funcvtions were called
	virtual void setFirstIter(){ first_iter = true; }
	//should be called after the first iteration on a new frame is done
	virtual void clearFirstIter(){ first_iter = false; }

	AMStatus is_initialized;
	PixStatus* isInitialized(){ return &is_initialized; }
	virtual void setInitStatus(){ is_initialized.set(); }
	virtual void clearInitStatus(){ is_initialized.clear(); }

	// ------------------------ Distance Feature ------------------------ //

	// computes the distance / dissimilarity between two patches where each is codified or represented 
	// by a suitable distance feature computed using updateDistFeat
	virtual double operator()(const double* a, const double* b, 
		size_t dist_size, double worst_dist = -1) const {
		am_not_implemeted(distance functor);
	}
	
	// to be called once during initialization if any of the distance feature functionality is to be used
	virtual void initializeDistFeat() {
		am_not_implemeted(initializeDistFeat);
	}
	// computes a "distance" vector using the current image patch such that,
	// when the distance vectors corresponding to two patches are passed to the distance operator above,
	// it uses these to compute a scalar that measures the distance or dissimilarity between the two patches; 
	// this distance vector should be designed so as to offload as much computation as possible from the 
	// distance operator, i.e. every computation that depends only on the current patch should be performed here
	// and the results should be stored, suitably coded, in the distannce vector 
	// where they will be decoded and used to compute the distance measure	
	virtual void updateDistFeat() {
		am_not_implemeted(updateDistFeat);
	}
	virtual const double* getDistFeat(){
		am_not_implemeted(getDistFeat);
	}
	// overloaded version to write the distance feature directly to a row (or column) of a matrix
	// storing the distance features corresponding to several patches;
	virtual void updateDistFeat(double* feat_addr) {
		am_not_implemeted(updateDistFeat);
	}
	// returns the size of the distance vector
	virtual int getDistFeatSize() {
		am_not_implemeted(getDistFeatSize);
	}
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

_MTF_END_NAMESPACE

#endif



