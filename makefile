include mtf_vars.mak
include mtf_flags.mak

all: mtfi

.PHONY: install
.PHONY: install_header
.PHONY: installd
.PHONY: dis
.PHONY: clean

mtf: ${MTF_LIB_NAME}
mtfr: ${MTF_EXE_NAME}
mtfi: install_header install ${MTF_EXE_NAME}
mtft: install ${MTF_TEST_EXE_NAME}
mtfp: install ${MTF_PY_NAME}
mtfc: clean
	rm -f ${MTF_EXE_NAME}
install: ${MTF_INSTALL_DIR}/${MTF_LIB_NAME}
install_header: ${MTF_HEADER_DIR} ${MTF_HEADERS}
#	@echo MTF_HEADERS=${MTF_HEADERS}
		
#@find . -type f -name '*.h' | cpio -p -d -v ${MTF_HEADER_DIR}/		
${MTF_HEADER_DIR}/%: %
	@sudo cp --parents $^ ${MTF_HEADER_DIR}
run: ${MTF_EXE_NAME}
	./${MTF_EXE_NAME}
clean: 
	rm -f ${BUILD_DIR}/*.o ./*.so
pfsl3: 	PF_SL3/PF_SL3.o
	
# --------------------------------------------------------------------- #
# ----------------------------- Utilities ----------------------------- #
# --------------------------------------------------------------------- #

${BUILD_DIR}/homUtils.o: homUtils.cc homUtils.h mtfMacros.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FLAGS64} homUtils.cc -o $@
	
${BUILD_DIR}/imgUtils.o: imgUtils.cc imgUtils.h homUtils.h mtfMacros.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FLAGS64} imgUtils.cc -o $@
	
${BUILD_DIR}/histUtils.o: histUtils.cc histUtils.h miscUtils.h mtfMacros.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${HIST_FLAGS} ${FLAGS64} histUtils.cc -o $@	
	
# -------------------------------------------------------------------------------------- #
# --------------------------------- State Space Models --------------------------------- #
# -------------------------------------------------------------------------------------- #
	
${BUILD_DIR}/ProjectiveBase.o: ProjectiveBase.cc ProjectiveBase.h StateSpaceModel.h mtfMacros.h homUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ProjectiveBase.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/LieHomography.o: LieHomography.cc LieHomography.h StateSpaceModel.h ProjectiveBase.h mtfMacros.h homUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} LieHomography.cc ${FLAGS64} -o $@

${BUILD_DIR}/CornerHomography.o: CornerHomography.cc CornerHomography.h StateSpaceModel.h ProjectiveBase.h mtfMacros.h homUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} CornerHomography.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/Homography.o: Homography.cc Homography.h StateSpaceModel.h ProjectiveBase.h mtfMacros.h homUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} Homography.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/Affine.o: Affine.cc Affine.h StateSpaceModel.h ProjectiveBase.h mtfMacros.h homUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} Affine.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/Similitude.o: Similitude.cc Similitude.h StateSpaceModel.h ProjectiveBase.h homUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ISO_FLAGS} Similitude.cc ${FLAGS64} -o $@	
	
${BUILD_DIR}/Isometry.o: Isometry.cc Isometry.h StateSpaceModel.h ProjectiveBase.h homUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ISO_FLAGS} Isometry.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/Transcaling.o: Transcaling.cc Transcaling.h StateSpaceModel.h ProjectiveBase.h homUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ISO_FLAGS} Transcaling.cc ${FLAGS64} -o $@	
	
${BUILD_DIR}/Translation.o: Translation.cc Translation.h StateSpaceModel.h ProjectiveBase.h homUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${TRANS_FLAGS} Translation.cc ${FLAGS64} -o $@
	
# ------------------------------------------------------------------------------------- #
# --------------------------------- Appearance Models --------------------------------- #
# ------------------------------------------------------------------------------------- #
	
${BUILD_DIR}/ImageBase.o: ImageBase.cc ImageBase.h mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} ImageBase.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/SSIM.o: SSIM.cc SSIM.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSIM_FLAGS} SSIM.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/SPSS.o: SPSS.cc SPSS.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SPSS_FLAGS} SPSS.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/NCC.o: NCC.cc NCC.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${NCC_FLAGS} NCC.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/SSDBase.o: SSDBase.cc SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} SSDBase.cc ${FLAGS64} -o $@	
	
${BUILD_DIR}/SSD.o: SSD.cc SSD.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} SSD.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/NSSD.o: NSSD.cc NSSD.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} NSSD.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/ZNCC.o: ZNCC.cc ZNCC.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} ZNCC.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/SCV.o: SCV.cc SCV.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} SCV.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/LSCV.o: LSCV.cc LSCV.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} ${LSCV_FLAGS} LSCV.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/RSCV.o: RSCV.cc RSCV.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} RSCV.cc ${FLAGS64} -o $@	
	
${BUILD_DIR}/LRSCV.o: LRSCV.cc LRSCV.h SSDBase.h AppearanceModel.h ImageBase.h  mtfMacros.h imgUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${SSD_FLAGS} ${LRSCV_FLAGS} LRSCV.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/KLD.o: KLD.cc KLD.h AppearanceModel.h ImageBase.h  mtfMacros.h histUtils.h imgUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${KLD_FLAGS} KLD.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/LKLD.o: LKLD.cc LKLD.h AppearanceModel.h ImageBase.h  mtfMacros.h histUtils.h imgUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${LKLD_FLAGS} LKLD.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/MI.o: MI.cc MI.h AppearanceModel.h ImageBase.h  mtfMacros.h histUtils.h imgUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${MI_FLAGS} MI.cc ${FLAGS64} -o $@
	
${BUILD_DIR}/CCRE.o: CCRE.cc CCRE.h AppearanceModel.h ImageBase.h  mtfMacros.h histUtils.h imgUtils.h miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${CCRE_FLAGS} CCRE.cc ${FLAGS64} -o $@		

# ------------------------------------------------------------------------------------ #
# ---------------------------------- Search Methods ---------------------------------- #
# ------------------------------------------------------------------------------------ #

${BUILD_DIR}/HACLK.o: HACLK.cc HACLK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${HAC_FLAGS} HACLK.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/ICLK.o: ICLK.cc ICLK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${IC_FLAGS} ICLK.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FCLK.o: FCLK.cc FCLK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FC_FLAGS} FCLK.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FALK.o: FALK.cc FALK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FA_FLAGS} FALK.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/IALK.o: IALK.cc IALK.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${IA_FLAGS} IALK.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/IALK2.o: IALK2.cc IALK2.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${IA_FLAGS} IALK2.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FCGD.o: FCGD.cc FCGD.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FC_FLAGS} FCGD.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/HESM.o: HESM.cc HESM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
		${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FC_FLAGS} HESM.cc ${FLAGS64} ${FLAGSCV} -o $@

${BUILD_DIR}/AESM.o: AESM.cc AESM.h ESM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ESM_FLAGS} AESM.cc ${FLAGS64} ${FLAGSCV} -o $@

${BUILD_DIR}/ESM.o: ESM.cc ESM.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${ESM_FLAGS} ESM.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/FESMBase.o: FESMBase.cc FESMBase.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FESMBase_FLAGS} FESMBase.cc ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/FESM.o: FESM.cc FESM.h FESMBase.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${FESM_FLAGS} FESM.cc ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/PF.o: PF.cc PF.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${PF_FLAGS} PF.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/NN.o: NN.cc NN.h ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${NN_FLAGS} NN.cc ${FLAGS64} ${FLAGSCV} -o $@
	
${BUILD_DIR}/GNN.o: GNN.cc GNN.h ${MTF_INSTALL_DIR}/libgnn.so ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${GNN_FLAGS} GNN.cc ${FLAGS64} ${FLAGSCV} -o $@	
${MTF_INSTALL_DIR}/libgnn.so: GNN/libgnn.so
	sudo cp -f $< $@
GNN/libgnn.so: GNN/build_graph.h GNN/build_graph.cc GNN/search_graph_knn.h GNN/search_graph_knn.cc GNN/utility.cc GNN/utility.h GNN/memwatch.cc GNN/memwatch.h 
	$(MAKE) -C ./GNN --no-print-directory
	
# ------------------------------------------------------------------------------------ #
# ---------------------------------- Composite ---------------------------------- #
# ------------------------------------------------------------------------------------ #
	
${BUILD_DIR}/GridTracker.o: GridTracker.cc GridTracker.h ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${GRID_FLAGS} GridTracker.cc ${FLAGS64} ${FLAGSCV} -o $@	
	
${BUILD_DIR}/ParallelTracker.o: ParallelTracker.cc ParallelTracker.h ${STATE_SPACE_HEADERS} ${BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ${PRL_FLAGS} ParallelTracker.cc ${FLAGS64} ${FLAGSCV} -o $@	
	
# ------------------------------------------------------------------------------------ #
# ---------------------------------- Diagnostics ---------------------------------- #
# ------------------------------------------------------------------------------------ #
	
${BUILD_DIR}/Diagnostics.o: Diagnostics.cc DiagAnalytic.cc DiagInvAnalytic.cc DiagNumeric.cc DiagHelper.cc ${DIAG_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${DIAG_BASE_HEADERS} miscUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} Diagnostics.cc ${FLAGS64} ${FLAGSCV} -o $@
	
# ------------------------------------------------------------------------------------- #
# ------------------------------------ Third Party ------------------------------------ #
# ------------------------------------------------------------------------------------- #
	
ThirdParty/DSST/DSST.o: ThirdParty/DSST/DSST.cpp ThirdParty/DSST/DSST.h ThirdParty/DSST/Params.h ThirdParty/DSST/HOG.h ThirdParty/DSST/sse.hpp ThirdParty/DSST/wrappers.h TrackerBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS_DSST} ${CT_FLAGS} ThirdParty/DSST/DSST.cpp ${FLAGS64} ${FLAGSCV} -o ThirdParty/DSST/DSST.o
	
ThirdParty/KCF/KCFTracker.o: ThirdParty/KCF/KCFTracker.cpp ThirdParty/KCF/KCFTracker.h ThirdParty/KCF/defines.h ThirdParty/DSST/HOG.h ThirdParty/DSST/sse.hpp ThirdParty/DSST/wrappers.h TrackerBase.h
	${CXX} -c -fPIC -I ./ThirdParty/DSST ${WARNING_FLAGS} ${OPT_FLAGS_DSST} ${CT_FLAGS} ThirdParty/KCF/KCFTracker.cpp ${FLAGS64} ${FLAGSCV} -o ThirdParty/KCF/KCFTracker.o
	
ThirdParty/RCT/CompressiveTracker.o: ThirdParty/RCT/CompressiveTracker.cpp ThirdParty/RCT/CompressiveTracker.h TrackerBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS_MTF} ${CT_FLAGS} ThirdParty/RCT/CompressiveTracker.cpp ${FLAGS64} ${FLAGSCV} -o ThirdParty/RCT/CompressiveTracker.o

${MTF_INSTALL_DIR}/libcmt.so: ThirdParty/CMT/libcmt.so
	sudo cp -f $< $@	
ThirdParty/CMT/libcmt.so: ThirdParty/CMT/CMT.cpp ThirdParty/CMT/CMT.h 
	cd ./ThirdParty/CMT; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .
	$(MAKE) -C ./ThirdParty/CMT --no-print-directory
	
${MTF_INSTALL_DIR}/libopentld.so: ThirdParty/TLD/libopentld.so
	sudo cp -f $< $@
ThirdParty/TLD/libopentld.so: ThirdParty/TLD/TLD.cpp ThirdParty/TLD/TLD.h
	cd ./ThirdParty/TLD; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .		
	$(MAKE) -C ./ThirdParty/TLD --no-print-directory	
${MTF_INSTALL_DIR}/libcvblobs.so: ThirdParty/TLD/3rdparty/cvblobs/libcvblobs.so
	sudo cp -f $< $@
ThirdParty/TLD/3rdparty/cvblobs/libcvblobs.so: ThirdParty/TLD/3rdparty/cvblobs/blob.h ThirdParty/TLD/3rdparty/cvblobs/blob.cpp
	cd ./ThirdParty/TLD/3rdparty/cvblobs; rm -rf ./CMakeCache.txt ./cmake_install.cmake ./CMakeFiles; cmake .
	$(MAKE) -C ./ThirdParty/TLD/3rdparty/cvblobs --no-print-directory
	
ThirdParty/PFSL3/PFSL3.o: ThirdParty/PFSL3/PFSL3.cc ThirdParty/PFSL3/PFSL3.h TrackerBase.h
	${CXX} -c -I /usr/include/clapack ${WARNING_FLAGS} ${OPT_FLAGS} -O3 PFSL3/PFSL3.cc ${FLAGS64} ${FLAGSCV} -o PFSL3/PFSL3.o	
ThirdParty/PFSL3/libpfsl3.a: 	PFSL3/PFSL3.o
	ar rcs PFSL3/libpfsl3.a PFSL3/PFSL3.o
	sudo cp -f PFSL3/libpfsl3.a /usr/lib
	
ThirdParty/ViSP/ViSP.o: ThirdParty/ViSP/ViSP.cc ThirdParty/ViSP/ViSP.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${PROF_FLAGS} ${CT_FLAGS} ThirdParty/ViSP/ViSP.cc ${FLAGS64} ${FLAGSCV} ${VISP_FLAGS} -o $@
	
# ------------------------------------------------------------------------------------- #
# ------------------------------------ Tools ------------------------------------ #
# ------------------------------------------------------------------------------------- #	
${MTF_HEADER_DIR}:
		sudo mkdir -p $@
${BUILD_DIR}/Tools:
		mkdir -p $@
${TOOLS_LIB_NAME}: ${BUILD_DIR}/Tools ${BUILD_DIR}/Tools/parameters.o ${BUILD_DIR}/Tools/inputCV.o ${BUILD_DIR}/Tools/inputBase.o ${BUILD_DIR}/Tools/cvUtils.o
	${CXX} -shared -o $@  ${BUILD_DIR}/Tools/parameters.o ${BUILD_DIR}/Tools/inputCV.o ${BUILD_DIR}/Tools/inputBase.o ${BUILD_DIR}/Tools/cvUtils.o
	sudo cp -f $@ /usr/lib	
${BUILD_DIR}/Tools/parameters.o: Tools/parameters.cc Tools/parameters.h 
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} ${FLAGS64} ${FLAGSCV} Tools/parameters.cc -o $@
${BUILD_DIR}/Tools/inputCV.o: Tools/inputCV.cc Tools/inputCV.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} ${FLAGS64} ${FLAGSCV}  ${MTF_FLAGSXV} Tools/inputCV.cc -o $@
${BUILD_DIR}/Tools/inputBase.o: Tools/inputBase.cc Tools/inputBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} ${FLAGS64} ${FLAGSCV}  ${MTF_FLAGSXV} Tools/inputBase.cc -o $@
${BUILD_DIR}/Tools/cvUtils.o: Tools/cvUtils.cc Tools/cvUtils.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} ${FLAGS64} ${FLAGSCV} Tools/cvUtils.cc -o $@	

# ---------------------------------------------------------------------------------------------- #
# ------------------------------------ Library & Executable ------------------------------------ #
# ---------------------------------------------------------------------------------------------- #
	
${BUILD_DIR}:
		mkdir -p $@	
		
${MTF_INSTALL_DIR}/${MTF_LIB_NAME}: ${MTF_LIB_NAME}
	sudo cp -f $< $@		
${MTF_LIB_NAME}:  ${BUILD_DIR} ${APPEARANCE_OBJS} ${STATE_SPACE_OBJS} ${SEARCH_OBJS}  ${LEARNING_OBJS} ${DIAG_OBJS} ${MTF_UTIL_OBJS} ${LEARNING_TRACKERS_SO}
	${CXX} -shared -o $@  ${LEARNING_OBJS} ${APPEARANCE_OBJS} ${SEARCH_OBJS} ${STATE_SPACE_OBJS}  ${DIAG_OBJS} ${MTF_UTIL_OBJS} ${LIBSCV} ${LIB_MTF_LIBS} ${VISP_LIBS}

${MTF_EXE_NAME}: runMTF.cc MTF.h ${SEARCH_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} ${MTF_UTIL_HEADERS} ${LEARNING_HEADERS} ${COMPOSITE_HEADERS} ${TOOL_HEADERS} ${XVISION_HEADERS}
	${CXX} -w ${WARNING_FLAGS} -o $@ ${OPT_FLAGS_MTF} ${RUN_MTF_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_FLAGSXV} runMTF.cc ${LIBS} ${LIBSCV} ${MTF_LIBSXV} ${LIBS_BOOST} ${LIBS_PARALLEL} ${MTF_LIB_LINK} -lstdc++ -lhdf5 ${FLAGS_LEARNING} ${LIBS_LEARNING} ${VISP_FLAGS} ${VISP_LIBS}
	
${MTF_TEST_EXE_NAME}: testMTF.cc MTF.h ${DIAG_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} ${MTF_UTIL_HEADERS} ${LEARNING_HEADERS} ${COMPOSITE_HEADERS} ${TOOL_HEADERS} ${XVISION_HEADERS}
	${CXX} -w ${WARNING_FLAGS} ${OPT_FLAGS_MTF} ${RUN_MTF_FLAGS} ${FLAGS64} ${FLAGSCV} ${MTF_FLAGSXV} testMTF.cc ${LIBS} ${LIBSCV} ${MTF_LIBSXV} ${LIBS_PARALLEL} ${LIBS_BOOST} -o $@ ${MTF_LIB_LINK} -lhdf5 ${FLAGS_LEARNING} ${LIBS_LEARNING} ${VISP_FLAGS} ${VISP_LIBS}

${MTF_PY_NAME}: ${BUILD_DIR}/pyMTF.o
	${CXX} -shared pyMTF.o -o $@  ${MTF_LIB_LINK} ${LIBS_BOOST} ${LIBS_PARALLEL} -lhdf5 ${LIBSCV} -lpython2.7  ${VISP_LIBS} ${LIBS_LEARNING} ${MTF_LIBSXV}
	
${BUILD_DIR}/pyMTF.o: pyMTF.cc  MTF.h  ${SEARCH_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${BASE_HEADERS} ${MTF_UTIL_HEADERS} ${LEARNING_HEADERS} ${COMPOSITE_HEADERS} ${TOOL_HEADERS} ${XVISION_HEADERS}
	${CXX} -w -c -fPIC pyMTF.cc ${WARNING_FLAGS} ${OPT_FLAGS_MTF} ${FLAGS64} ${FLAGSCV} ${RUN_MTF_FLAGS} ${MTF_FLAGSXV} -I/usr/include/python2.7 -I/usr/include/python2.7/numpy ${FLAGS_LEARNING} ${VISP_FLAGS} -o $@ 
	
