#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

SSDBase::SSDBase(ImgParams *img_params) :
AppearanceModel(img_params),
curr_pix_diff(0, 0){
	pix_norm_mult = 1;
	pix_norm_add = 0;
}

void SSDBase::initialize(){
	if(is_initialized.similarity)
		return;

	init_grad.resize(n_pix);
	new (&curr_pix_diff) VectorXdM(init_grad.data(), n_pix);
	curr_pix_diff.fill(0);
	similarity = 0;
	is_initialized.similarity = true;
}

void SSDBase::initializeGrad(){
	if(is_initialized.grad)
		return;

	curr_grad.resize(n_pix);
	setCurrGrad(getInitGrad());
	is_initialized.grad = true;
}

void SSDBase::cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
	const MatrixXd &curr_pix_hessian){
	int ssm_state_size = curr_hessian.rows();

	assert(curr_hessian.cols() == ssm_state_size);
	assert(curr_pix_hessian.rows() == ssm_state_size * ssm_state_size);

	cmptCurrHessian(curr_hessian, curr_pix_jacobian);
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
#ifndef DISABLE_SPI
		if(spi_mask && !spi_mask[pix_id]){ continue; }
#endif
		curr_hessian += Map<MatrixXd>((double*)curr_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size) * curr_grad(pix_id);
	}
}
void SSDBase::cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
	const MatrixXd &init_pix_hessian){
	int ssm_state_size = init_hessian.rows();
	assert(init_hessian.cols() == ssm_state_size);
	assert(init_pix_hessian.rows() == ssm_state_size * ssm_state_size);

	cmptInitHessian(init_hessian, init_pix_jacobian);
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
#ifndef DISABLE_SPI
		if(spi_mask && !spi_mask[pix_id]){ continue; }
#endif
		init_hessian += Map<MatrixXd>((double*)init_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size) * init_grad(pix_id);
	}
}

void SSDBase::cmptSumOfHessians(MatrixXd &sum_of_hessians,
	const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian,
	const MatrixXd &init_pix_hessian, const MatrixXd &curr_pix_hessian){

	int ssm_state_size = sum_of_hessians.rows();
	assert(sum_of_hessians.cols() == ssm_state_size);
	assert(init_pix_hessian.rows() == ssm_state_size * ssm_state_size);

	cmptSumOfHessians(sum_of_hessians, init_pix_jacobian, curr_pix_jacobian);
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
#ifndef DISABLE_SPI
		if(spi_mask && !spi_mask[pix_id]){ continue; }
#endif
		sum_of_hessians += init_grad(pix_id)*
			(Map<MatrixXd>((double*)init_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size)
			+ Map<MatrixXd>((double*)curr_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size));
	}
}

void SSDBase::getJacobian(RowVectorXd &jacobian, const bool *pix_mask,
	const RowVectorXd &curr_grad, const MatrixXd &pix_jacobian){
	assert(pix_jacobian.rows() == n_pix && pix_jacobian.rows() == jacobian.size());
	int state_vec_size = pix_jacobian.cols();
	jacobian.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id])
			continue;
		jacobian += curr_grad[pix_id] * pix_jacobian.row(pix_id);
	}
}

void SSDBase::getDifferenceOfJacobians(RowVectorXd &diff_of_jacobians, const bool *pix_mask,
	const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
	assert(init_pix_jacobian.rows() == n_pix && curr_pix_jacobian.rows() == n_pix);
	assert(init_pix_jacobian.rows() == diff_of_jacobians.size());

	diff_of_jacobians.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id])
			continue;
		diff_of_jacobians += curr_grad[pix_id] *
			(init_pix_jacobian.row(pix_id) + curr_pix_jacobian.row(pix_id));
	}
}

void SSDBase::getHessian(MatrixXd &hessian, const bool *pix_mask, const MatrixXd &pix_jacobian){
	assert(pix_jacobian.rows() == n_pix);

	hessian.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id])
			continue;
		hessian -= pix_jacobian.row(pix_id).transpose()*pix_jacobian.row(pix_id);
	}
}

void SSDBase::getSumOfHessians(MatrixXd &sum_of_hessians, const bool *pix_mask,
	const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
	assert(init_pix_jacobian.rows() == n_pix && curr_pix_jacobian.rows() == n_pix);
	assert(sum_of_hessians.rows() == sum_of_hessians.cols() && sum_of_hessians.rows() == init_pix_jacobian.cols());

	sum_of_hessians.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		if(!pix_mask[pix_id])
			continue;
		sum_of_hessians -= init_pix_jacobian.row(pix_id).transpose()*init_pix_jacobian.row(pix_id)
			+ curr_pix_jacobian.row(pix_id).transpose()*curr_pix_jacobian.row(pix_id);
	}
}

/**
* Squared Euclidean distance functor, optimized version
*/
/**
*  Compute the squared Euclidean distance between two vectors.
*
*	This is highly optimized, with loop unrolling, as it is one
*	of the most expensive inner loops.
*
*	The computation of squared root at the end is omitted for
*	efficiency.
*/
double SSDBase::operator()(const double* a, const double* b, 
	size_t size, double worst_dist) const{
	double result = double();
	double diff0, diff1, diff2, diff3;
	const double* last = a + size;
	const double* lastgroup = last - 3;

	/* Process 4 items with each loop for efficiency. */
	while(a < lastgroup) {
		diff0 = (double)(a[0] - b[0]);
		diff1 = (double)(a[1] - b[1]);
		diff2 = (double)(a[2] - b[2]);
		diff3 = (double)(a[3] - b[3]);
		result += diff0 * diff0 + diff1 * diff1 + diff2 * diff2 + diff3 * diff3;
		a += 4;
		b += 4;

		if((worst_dist > 0) && (result > worst_dist)) {
			return result;
		}
	}
	/* Process last 0-3 pixels.  Not needed for standard vector lengths. */
	while(a < last) {
		diff0 = (double)(*a++ - *b++);
		result += diff0 * diff0;
	}
	return result;
}

_MTF_END_NAMESPACE

