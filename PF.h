#ifndef PF_H
#define PF_H

#include "SearchMethod.h"
#include "miscUtils.h"

#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#define PF_MAX_ITERS 10
#define PF_N_PARTICLES 200
#define PF_UPD_THRESH 0.01
#define PF_DYN_MODEL 0
#define PF_LIKELIHOOD_FUNC 0
#define PF_RESAMPLING_TYPE 0
#define PF_RESET_TO_MEAN false
#define PF_MEAN_OF_CORNERS false
#define PF_CORNER_SIGMA_D 0.06
#define PF_PIX_SIGMA 0.04
#define PF_MEASUREMENT_SIGMA 0.1
#define PF_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct PFParams{
	// supported dynamic models for sample generation
	enum class DynamicModel{
		RandomWalk,
		AutoRegression1
	};
	enum class ResamplingType{
		None,
		BinaryMultinomial,
		LinearMultinomial,
		Residual
	};
	enum class LikelihoodFunc{
		AM,
		Gaussian,
		Reciprocal
	};

	int max_iters; //! maximum iterations of the PF algorithm to run for each frame
	int n_particles;
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations	
	DynamicModel dyn_model;
	LikelihoodFunc likelihood_func;
	ResamplingType resampling_type;
	bool reset_to_mean;
	// instead of computing the mean of SSM state samples, get the corners 
	// corresponding to each sample and take the mean of these instead;
	bool mean_of_corners;

	vector<double> ssm_sigma;
	double pix_sigma;
	double measurement_sigma;

	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	const char* operator()(DynamicModel _dyn_model) const{
		switch(_dyn_model){
		case DynamicModel::RandomWalk:
			return "RandomWalk";
		case DynamicModel::AutoRegression1:
			return "AutoRegression1";
		default:
			throw std::invalid_argument("Invalid dynamic model provided");
		}
	}
	const char* operator()(ResamplingType _resampling_type) const{
		switch(_resampling_type){
		case ResamplingType::None:
			return "None";
		case ResamplingType::BinaryMultinomial:
			return "BinaryMultinomial";
		case ResamplingType::LinearMultinomial:
			return "LinearMultinomial";
		case ResamplingType::Residual:
			return "Residual";
		default:
			throw std::invalid_argument("Invalid resampling type provided");
		}
	}
	const char* operator()(LikelihoodFunc _likelihood_func) const{
		switch(_likelihood_func){
		case LikelihoodFunc::AM:
			return "AM";
		case LikelihoodFunc::Gaussian:
			return "Gaussian";
		case LikelihoodFunc::Reciprocal:
			return "Reciprocal";
		default:
			throw std::invalid_argument("Invalid likelihood function provided");
		}
	}
	PFParams(int _max_iters, int _n_particles, double _upd_thresh,
		DynamicModel _dyn_model, LikelihoodFunc _likelihood_func,
		ResamplingType _resampling_type,
		bool _reset_to_mean, bool _mean_of_corners,
		const vector<double> &_ssm_sigma,
		double _pix_sigma, double _measurement_sigma, 
		bool _debug_mode){
		max_iters = _max_iters;
		n_particles = _n_particles;
		upd_thresh = _upd_thresh;
		dyn_model = _dyn_model;
		likelihood_func = _likelihood_func;
		resampling_type = _resampling_type;
		reset_to_mean = _reset_to_mean;
		mean_of_corners = _mean_of_corners;
		ssm_sigma = _ssm_sigma;
		pix_sigma = _pix_sigma;
		measurement_sigma = _measurement_sigma;
		debug_mode = _debug_mode;
	}

	PFParams(PFParams *params = nullptr) :
		max_iters(PF_MAX_ITERS),
		n_particles(PF_N_PARTICLES),
		upd_thresh(PF_UPD_THRESH),
		dyn_model(static_cast<DynamicModel>(PF_DYN_MODEL)),
		likelihood_func(static_cast<LikelihoodFunc>(PF_LIKELIHOOD_FUNC)),
		resampling_type(static_cast<ResamplingType>(PF_RESAMPLING_TYPE)),
		reset_to_mean(PF_RESET_TO_MEAN),
		mean_of_corners(PF_MEAN_OF_CORNERS),
		pix_sigma(PF_PIX_SIGMA),
		measurement_sigma(PF_MEASUREMENT_SIGMA),
		debug_mode(PF_DEBUG_MODE){
		if(params){
			max_iters = params->max_iters;
			n_particles = params->n_particles;
			upd_thresh = params->upd_thresh;
			dyn_model = params->dyn_model;
			likelihood_func = params->likelihood_func;
			resampling_type = params->resampling_type;
			reset_to_mean = params->reset_to_mean;
			mean_of_corners = params->mean_of_corners;
			ssm_sigma = params->ssm_sigma;
			pix_sigma = params->pix_sigma;
			measurement_sigma = params->measurement_sigma;
			debug_mode = params->debug_mode;
		}
	}
};

struct Particle{
	VectorXd state[2];
	VectorXd ar[2];
	double wt;
	double cum_wt;
	int curr_set_id;
	void resize(int state_size){
		state[0].resize(state_size);
		state[1].resize(state_size);
		ar[0].resize(state_size);
		ar[1].resize(state_size);
	}
};

// Particle Filter
template<class AM, class SSM>
class PF : public SearchMethod < AM, SSM > {
private:
	char *log_fname;
	char *time_fname;

public:

	// similarity of the initial patch (or template) with itself
	double max_similarity;

	typedef PFParams ParamType;
	ParamType params;

	typedef ParamType::DynamicModel DynamicModel;
	typedef ParamType::LikelihoodFunc LikelihoodFunc;
	typedef ParamType::ResamplingType ResamplingType;

	typedef boost::minstd_rand RandGenT;
	typedef boost::normal_distribution<double> MeasureDistT;
	//typedef boost::variate_generator<RandGenT&, MeasureDistT> MeasureGenT;
	typedef boost::random::uniform_real_distribution<double> ResampleDistT;
	//typedef boost::variate_generator<RandGenT&, ResampleDistT> ResampleGenT;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::cv_corners;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	RandGenT measurement_gen;
	MeasureDistT measurement_dist;

	RandGenT resample_gen;
	ResampleDistT resample_dist;
	typedef ResampleDistT::param_type ResampleDistParamT;

	int frame_id;

	Matrix3d warp_update;

	CornersT mean_corners;
	// corners corresponding to all the SSM states
	std::vector<CornersT> particle_corners;
	CornersT prev_corners;

	VectorXd mean_state;
	// SSM states for all particles
	// 2 sets of particles are stored for efficient resampling
	std::vector<VectorXd> particle_states[2];
	// Update history for Auto Regression
	std::vector<VectorXd> particle_ar[2];
	int curr_set_id;
	VectorXd particle_wts;
	VectorXd particle_cum_wts;

	VectorXd perturbed_state;
	VectorXd perturbed_ar;

	VectorXd state_sigma;

	double measurement_likelihood;
	double measurement_factor;

	PF(ParamType *pf_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	~PF(){}

	void initialize(const cv::Mat &corners) override;
	void update() override;

	void linearMultinomialResampling();
	void binaryMultinomialResampling();
	void residualResampling();


	void convertCornersToPoint2D(cv::Point2d *corners,
		const CornersT &eig_corners){
		for(int i = 0; i < 4; i++){
			corners[i].x = eig_corners(0, i);
			corners[i].y = eig_corners(1, i);
		}
	}

	void initializeParticles();
	void updateMeanCorners(const VectorXd &particle_state, int particle_id){
		// compute running average of corners corresponding to the resampled particle states
		ssm->setState(particle_state);
		mean_corners += (ssm->getCorners() - mean_corners) / (particle_id + 1);
	}

	//const cv::Mat& getRegion() override;

	void setRegion(const cv::Mat& corners) override{
		ssm->setCorners(corners);
		ssm->getCorners(cv_corners);
		initializeParticles();
	}
};

_MTF_END_NAMESPACE

#endif

