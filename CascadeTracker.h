#ifndef CASCADE_TRACKER_H
#define CASCADE_TRACKER_H

#include "TrackerBase.h"
#include "miscUtils.h"

#define CASC_ENABLE_FEEDBACK true

_MTF_BEGIN_NAMESPACE

struct CascadeParams{
	bool enable_feedback;
	CascadeParams(bool _enable_feedback){
		enable_feedback = _enable_feedback;
	}
	CascadeParams(CascadeParams *params = nullptr) :
		enable_feedback(CASC_ENABLE_FEEDBACK){
		if(params){
			enable_feedback = params->enable_feedback;
		}
	}
};

class CascadeTracker : public TrackerBase {

public:

	typedef CascadeParams ParamType;
	ParamType params;

	const vector<TrackerBase*> trackers;
	int n_trackers;

	CascadeTracker(const vector<TrackerBase*> _trackers, ParamType *casc_params) :
		TrackerBase(), params(casc_params), trackers(_trackers){
		n_trackers = trackers.size();
		
		printf("\n");
		printf("Initializing Cascade tracker with:\n");
		printf("n_trackers: %d\n", n_trackers);
		printf("trackers: ");
		for(int i = 0; i < n_trackers; i++){
			name = name + trackers[i]->name + " ";
			printf("%d: %s ", i + 1, trackers[i]->name.c_str());
		}
		printf("\n");

		if(params.enable_feedback){
			printf("Feedback is enabled\n");
		}
	}

	void initialize(const cv::Mat &img, const cv::Mat &corners) override{
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			trackers[tracker_id]->initialize(img, corners);
		}
		convertMatToPoint2D(cv_corners, getRegion());
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
	}

	void initialize(const cv::Mat &corners) override{
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			//printf("Initializing tracker %d\n", tracker_id);
			trackers[tracker_id]->initialize(corners);
		}
		convertMatToPoint2D(cv_corners, getRegion());
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
	}
	void update() override{
		trackers[0]->update();
		for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
			//printf("tracker: %d ", tracker_id - 1);
			//utils::printMatrix<double>(trackers[tracker_id - 1]->getRegion(),
			//	"region is: ");
			trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
			//printf("tracker: %d ", tracker_id);
			//utils::printMatrix<double>(trackers[tracker_id]->getRegion(),
			//	"region set to: ");
			trackers[tracker_id]->update();
		}
		if(params.enable_feedback){
			trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
		}		
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
		convertMatToPoint2D(cv_corners, getRegion());
	}
	void update(const cv::Mat &img) override{
		trackers[0]->update(img);
		for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
			trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
			trackers[tracker_id]->update(img);
		}
		if(params.enable_feedback){
			trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
		}
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
		convertMatToPoint2D(cv_corners, getRegion());
	}

	const cv::Mat& getRegion()  override{ return trackers[n_trackers - 1]->getRegion(); }

	void setRegion(const cv::Mat& corners)  override{
		for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
			trackers[tracker_id]->setRegion(corners);
		}
	}

private:
	void convertMatToPoint2D(cv::Point2d *cv_corners, 
		const cv::Mat &cv_corners_mat){
		for(int i = 0; i < 4; i++){
			cv_corners[i].x = cv_corners_mat.at<double>(0, i);
			cv_corners[i].y = cv_corners_mat.at<double>(1, i);
		}
	}
};
_MTF_END_NAMESPACE

#endif

