#include "GridTracker.h"

#include "miscUtils.h"
#include <vector>
#include <stdexcept>
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "tbb/tbb.h" 


_MTF_BEGIN_NAMESPACE


GridTrackerParams::GridTrackerParams(int _patch_size_x, int _patch_size_y,
EstType _estimation_method,
double _ransac_reproj_thresh, bool _init_at_each_frame,
bool _dyn_patch_size, bool _use_tbb,
int _max_iters, double _upd_thresh,
bool _show_trackers, bool _show_tracker_edges,
bool _debug_mode){
	patch_size_x = _patch_size_x;
	patch_size_y = _patch_size_y;
	estimation_method = _estimation_method;
	ransac_reproj_thresh = _ransac_reproj_thresh;
	init_at_each_frame = _init_at_each_frame;
	dyn_patch_size = _dyn_patch_size;
	use_tbb = _use_tbb;
	max_iters = _max_iters;
	upd_thresh = _upd_thresh;
	show_trackers = _show_trackers;
	show_tracker_edges = _show_tracker_edges;
	debug_mode = _debug_mode;
}
GridTrackerParams::GridTrackerParams(GridTrackerParams *params) :
patch_size_x(GT_PATCH_SIZE_X),
patch_size_y(GT_PATCH_SIZE_Y),
estimation_method(static_cast<EstType>(GT_ESTIMATION_METHOD)),
ransac_reproj_thresh(GT_RANSAC_REPROJ_THRESH),
init_at_each_frame(GT_INIT_AT_EACH_FRAME),
dyn_patch_size(GT_DYN_PATCH_SIZE),
use_tbb(GT_USE_TBB),
max_iters(GT_MAX_ITERS), upd_thresh(GT_UPD_THRESH),
show_trackers(GT_SHOW_TRACKERS),
show_tracker_edges(GT_SHOW_TRACKER_EDGES),
debug_mode(GT_DEBUG_MODE){
	if(params){
		patch_size_x = params->patch_size_x;
		patch_size_y = params->patch_size_y;
		estimation_method = params->estimation_method;
		ransac_reproj_thresh = params->ransac_reproj_thresh;
		init_at_each_frame = params->init_at_each_frame;
		dyn_patch_size = params->dyn_patch_size;
		use_tbb = params->use_tbb;
		max_iters = params->max_iters;
		upd_thresh = params->upd_thresh;
		show_trackers = params->show_trackers;
		show_tracker_edges = params->show_tracker_edges;
		debug_mode = params->debug_mode;
	}
}
template<class SSM>
GridTracker<SSM>::GridTracker(int resx, int resy, SSMParams *ssm_params, ParamType *grid_params,
	vector<TrackerBase*> _trackers, const cv::Mat &cv_img) :
	SearchMethod<void, SSM>(resx, resy, ssm_params),
	params(grid_params),
	grid_size_x(resx), grid_size_y(resy){
	printf("\n");
	printf("Initializing Grid tracker with:\n");
	printf("grid_size: %d x %d\n", grid_size_x, grid_size_y);
	printf("patch_size: %d x %d\n", params.patch_size_x, params.patch_size_y);
	printf("estimation_method: %d\n", params.estimation_method);
	printf("ransac_reproj_thresh: %f\n", params.ransac_reproj_thresh);
	printf("init_at_each_frame: %d\n", params.init_at_each_frame);
	printf("use_tbb: %d\n", params.use_tbb);
	printf("max_iters: %d\n", params.max_iters);
	printf("show_trackers: %d\n", params.show_trackers);
	printf("show_tracker_edges: %d\n", params.show_tracker_edges);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("\n");

	name = "grid";

	trackers = _trackers;
	n_trackers = grid_size_x*grid_size_y;

	if(trackers.size() != n_trackers){
		printf("No. of trackers provided: %lu\n", trackers.size());
		printf("No. of trackers needed for the grid: %d\n", n_trackers);
		throw std::invalid_argument("GridTracker :: Mismatch between grid dimensions and no. of trackers");
	}
	switch(params.estimation_method){
	case EstType::LeastSquares:
		printf("Using LeastSquares estimation\n");
		estimation_method_cv = 0;
		break;
	case EstType::RANSAC:
		printf("Using RANSAC estimation\n");
		estimation_method_cv = CV_RANSAC;
		break;
	case EstType::LeastMedian:
		printf("Using LeastMedian estimation\n");
		estimation_method_cv = CV_LMEDS;
		break;
	default:
		throw std::invalid_argument("GridTracker :: Invalid estimation method specified");
	}

	if(params.dyn_patch_size){
		printf("Using dynamic patch sizes\n");
		int sub_regions_x = grid_size_x + 1, sub_regions_y = grid_size_y + 1;
		if(ssm){ delete(ssm); }
		ssm = new SSM(sub_regions_x, sub_regions_y, ssm_params);
		_linear_idx.resize(sub_regions_y, sub_regions_x);
		for(int idy = 0; idy < sub_regions_y; idy++){
			for(int idx = 0; idx < sub_regions_x; idx++){
				_linear_idx(idy, idx) = idy * sub_regions_x + idx;
			}
		}
	}

	patch_corners.create(2, 4, CV_64FC1);
	cv_corners_mat.create(2, 4, CV_64FC1);
	prev_pts.resize(n_trackers);
	curr_pts.resize(n_trackers);
	ssm_update.resize(ssm->getStateSize());
	centrod_dist_x = params.patch_size_x / 2.0;
	centrod_dist_y = params.patch_size_y / 2.0;

	pix_mask.resize(n_trackers);
	std::fill(pix_mask.begin(), pix_mask.end(), 1);
	pause_seq = 0;

	//centroid_offset.resize(Eigen::NoChange, n_trackers);
	//int tracker_id = 0;
	//VectorXd x_offsets = VectorXd::LinSpaced(resx, centrod_dist_x, -centrod_dist_x);
	//VectorXd y_offsets = VectorXd::LinSpaced(resy, centrod_dist_y, -centrod_dist_y);
	//for(int row_id = 0; row_id < resy; row_id++){
	//	//double offset_y = row_id == 0 ? patch_centrod_dist
	//	//	: row_id == resy - 1 ? -patch_centrod_dist
	//	//	: 0;
	//	for(int col_id = 0; col_id < resx; col_id++){
	//		//double offset_x = col_id == 0 ? patch_centrod_dist
	//		//	: col_id == resx - 1 ? -patch_centrod_dist
	//		//	: 0;
	//		centroid_offset(0, tracker_id) = x_offsets(col_id);
	//		centroid_offset(1, tracker_id) = y_offsets(row_id);
	//		tracker_id++;
	//	}
	//}

	if(params.show_trackers){
		patch_img = cv_img;
		patch_img_uchar.create(cv_img.rows, cv_img.cols, CV_8UC3);
		patch_win_name = "Patch Trackers";
		cv::namedWindow(patch_win_name);
	}
}
template<class SSM>
void GridTracker<SSM>::initialize(const cv::Mat &img, const cv::Mat &corners) {
	patch_img = img;
	initialize(corners);
}
template<class SSM>
void GridTracker<SSM>::update(const cv::Mat &img) {
	patch_img = img;
	update();
}

template<class SSM>
void GridTracker<SSM>::initialize(const cv::Mat &corners) {
	ssm->initialize(corners);
	initTrackers();
	ssm->getCorners(cv_corners);
	if(params.show_trackers){
		patch_img.convertTo(patch_img_uchar, patch_img_uchar.type());
		cv::cvtColor(patch_img_uchar, patch_img_uchar, CV_GRAY2BGR);
		cv::Scalar line_color(255, 0, 0);
		line(patch_img_uchar, cv_corners[0], cv_corners[1], line_color, 2);
		line(patch_img_uchar, cv_corners[1], cv_corners[2], line_color, 2);
		line(patch_img_uchar, cv_corners[2], cv_corners[3], line_color, 2);
		line(patch_img_uchar, cv_corners[3], cv_corners[0], line_color, 2);
		cv::Scalar tracker_color(0, 255, 0);
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
			circle(patch_img_uchar, prev_pts[tracker_id], 2, tracker_color, 2);
			if(params.show_tracker_edges){
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[0], trackers[tracker_id]->cv_corners[1], tracker_color, 1);
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[1], trackers[tracker_id]->cv_corners[2], tracker_color, 1);
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[2], trackers[tracker_id]->cv_corners[3], tracker_color, 1);
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[3], trackers[tracker_id]->cv_corners[0], tracker_color, 1);
			}
		}
		imshow(patch_win_name, patch_img_uchar);
		//int key = cv::waitKey(1-pause_seq);
		//if(key == 32){
		//	pause_seq = 1 - pause_seq;
		//}
	}
}
template<class SSM>
void GridTracker<SSM>::update() {
	updateTrackers();
	ssm->estimateWarpFromPts(ssm_update, pix_mask, prev_pts, curr_pts,
		estimation_method_cv, params.ransac_reproj_thresh);
	Matrix24d opt_warped_corners;
	ssm->applyWarpToCorners(opt_warped_corners, ssm->getCorners(), ssm_update);
	ssm->setCorners(opt_warped_corners);

	if(params.init_at_each_frame){
		initTrackers();
	} else {
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			prev_pts[tracker_id].x = curr_pts[tracker_id].x;
			prev_pts[tracker_id].y = curr_pts[tracker_id].y;
		}
	}
	ssm->getCorners(cv_corners);
	if(params.show_trackers){
		patch_img.convertTo(patch_img_uchar, patch_img_uchar.type());
		cv::cvtColor(patch_img_uchar, patch_img_uchar, CV_GRAY2BGR);
		cv::Scalar line_color(255, 0, 0);
		line(patch_img_uchar, cv_corners[0], cv_corners[1], line_color, 2);
		line(patch_img_uchar, cv_corners[1], cv_corners[2], line_color, 2);
		line(patch_img_uchar, cv_corners[2], cv_corners[3], line_color, 2);
		line(patch_img_uchar, cv_corners[3], cv_corners[0], line_color, 2);
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
			cv::Scalar tracker_color;
			if(pix_mask[tracker_id]){
				tracker_color = cv::Scalar(0, 255, 0);
			} else{
				tracker_color = cv::Scalar(0, 0, 255);
			}
			circle(patch_img_uchar, curr_pts[tracker_id], 2, tracker_color, 2);
			if(params.show_tracker_edges){
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[0], trackers[tracker_id]->cv_corners[1], tracker_color, 1);
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[1], trackers[tracker_id]->cv_corners[2], tracker_color, 1);
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[2], trackers[tracker_id]->cv_corners[3], tracker_color, 1);
				line(patch_img_uchar, trackers[tracker_id]->cv_corners[3], trackers[tracker_id]->cv_corners[0], tracker_color, 1);
			}
		}
		imshow(patch_win_name, patch_img_uchar);
		//int key = cv::waitKey(1 - pause_seq);
		//if(key == 32){
		//	pause_seq = 1 - pause_seq;
		//}
	}
}

template<class SSM>
void GridTracker<SSM>::setRegion(const cv::Mat& corners) {
	ssm->setCorners(corners);
	if(params.init_at_each_frame){
		initTrackers();
	} else {
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			prev_pts[tracker_id].x = ssm->getPts()(0, tracker_id);
			prev_pts[tracker_id].y = ssm->getPts()(1, tracker_id);
		}
	}
	ssm->getCorners(cv_corners);
}

template<class SSM>
void GridTracker<SSM>::initTracker(int tracker_id){
	if(params.dyn_patch_size){
		int row_id = tracker_id / grid_size_x;
		int col_id = tracker_id % grid_size_x;

		patch_corners.at<double>(0, 0) = ssm->getPts()(0, _linear_idx(row_id, col_id));
		patch_corners.at<double>(1, 0) = ssm->getPts()(1, _linear_idx(row_id, col_id));

		patch_corners.at<double>(0, 1) = ssm->getPts()(0, _linear_idx(row_id, col_id + 1));
		patch_corners.at<double>(1, 1) = ssm->getPts()(1, _linear_idx(row_id, col_id + 1));

		patch_corners.at<double>(0, 2) = ssm->getPts()(0, _linear_idx(row_id + 1, col_id + 1));
		patch_corners.at<double>(1, 2) = ssm->getPts()(1, _linear_idx(row_id + 1, col_id + 1));

		patch_corners.at<double>(0, 3) = ssm->getPts()(0, _linear_idx(row_id + 1, col_id));
		patch_corners.at<double>(1, 3) = ssm->getPts()(1, _linear_idx(row_id + 1, col_id));

		trackers[tracker_id]->initialize(patch_corners);

		//patch_corners = trackers[tracker_id]->getRegion();
		prev_pts[tracker_id].x = (trackers[tracker_id]->cv_corners[0].x + trackers[tracker_id]->cv_corners[1].x
			+ trackers[tracker_id]->cv_corners[2].x + trackers[tracker_id]->cv_corners[3].x) / 4.0;
		prev_pts[tracker_id].y = (trackers[tracker_id]->cv_corners[0].y + trackers[tracker_id]->cv_corners[1].y
			+ trackers[tracker_id]->cv_corners[2].y + trackers[tracker_id]->cv_corners[3].y) / 4.0;

	} else{
		Vector2d patch_centroid = ssm->getPts().col(tracker_id);
		double min_x = patch_centroid(0) - centrod_dist_x;
		double max_x = patch_centroid(0) + centrod_dist_x;
		double min_y = patch_centroid(1) - centrod_dist_y;
		double max_y = patch_centroid(1) + centrod_dist_y;

		patch_corners.at<double>(0, 0) = patch_corners.at<double>(0, 3) = min_x;
		patch_corners.at<double>(0, 1) = patch_corners.at<double>(0, 2) = max_x;
		patch_corners.at<double>(1, 0) = patch_corners.at<double>(1, 1) = min_y;
		patch_corners.at<double>(1, 2) = patch_corners.at<double>(1, 3) = max_y;

		trackers[tracker_id]->initialize(patch_corners);

		prev_pts[tracker_id].x = patch_centroid(0);
		prev_pts[tracker_id].y = patch_centroid(1);
	}
}

template<class SSM>
void GridTracker<SSM>::updateTracker(int tracker_id){
	trackers[tracker_id]->update();
	//patch_corners = trackers[tracker_id]->getRegion();
	curr_pts[tracker_id].x = (trackers[tracker_id]->cv_corners[0].x + trackers[tracker_id]->cv_corners[1].x
		+ trackers[tracker_id]->cv_corners[2].x + trackers[tracker_id]->cv_corners[3].x) / 4.0;
	curr_pts[tracker_id].y = (trackers[tracker_id]->cv_corners[0].y + trackers[tracker_id]->cv_corners[1].y
		+ trackers[tracker_id]->cv_corners[2].y + trackers[tracker_id]->cv_corners[3].y) / 4.0;
}

template<class SSM>
void GridTracker<SSM>::initTrackers(){
	if(params.use_tbb){
		parallel_for(tbb::blocked_range<size_t>(0, n_trackers),
			[&](const tbb::blocked_range<size_t>& r){
			for(size_t tracker_id = r.begin(); tracker_id != r.end(); ++tracker_id){
				initTracker(tracker_id);
			}
		}
		);
	} else{
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			initTracker(tracker_id);
		}
	}
}
template<class SSM>
void GridTracker<SSM>::updateTrackers(){
	if(params.use_tbb){
		parallel_for(tbb::blocked_range<size_t>(0, n_trackers),
			[&](const tbb::blocked_range<size_t>& r){
			for(size_t tracker_id = r.begin(); tracker_id != r.end(); ++tracker_id) updateTracker(tracker_id);
		}
		);

	} else{
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++)	updateTracker(tracker_id);
	}
}

_MTF_END_NAMESPACE
#include "mtfRegister.h"
_REGISTER_TRACKERS_SSM(GridTracker);