#ifndef NCC_H
#define NCC_H

#include "AppearanceModel.h"

_MTF_BEGIN_NAMESPACE

// Normalized Cross Correlation
class NCC : public AppearanceModel{
public:
	typedef ImgParams ParamType;
	ParamType params;

	//! mean of the initial and current pixel values
	double init_pix_mean, curr_pix_mean;
	double a, b, c;
	double bc, b2c;

	bool enable_pix_proc;

	VectorXd init_pix_vals_cntr, curr_pix_vals_cntr;
	VectorXd init_pix_vals_cntr_c, curr_pix_vals_cntr_b;
	VectorXd init_grad_ncntr, curr_grad_ncntr;
	double init_grad_ncntr_mean, curr_grad_ncntr_mean;

	NCC(ParamType *ncc_params);

	double getLikelihood() override{
		return exp(similarity-1);
	}

	//-------------------------------initialize functions------------------------------------//
	void initialize() override;
	void initializeGrad() override;
	void initializeHess() override {}

	//-------------------------------update functions------------------------------------//
	void update(bool prereq_only = true) override;
	void updateInitGrad() override;
	// nothing is done here since curr_grad is same as and shares memory with  curr_pix_diff
	void updateCurrGrad() override;

	void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian) override;
	void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
		const MatrixXd &init_pix_hessian) override;

	void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian) override;
	void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override;

	void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian) override;
	void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override{
		cmptSelfHessian(self_hessian, curr_pix_jacobian);
	}

	/*Support for FLANN library*/
	VectorXd curr_feat_vec;
	NCC() : AppearanceModel(){}
	typedef double ElementType;
	typedef double ResultType;
	double operator()(const double* a, const double* b, 
		size_t size, double worst_dist = -1) const override;
	void updateDistFeat(double* feat_addr) override;
	void updateDistFeat() override{
		updateDistFeat(curr_feat_vec.data());
	}
	int  getDistFeatSize() override{ return n_pix + 1; }
	void initializeDistFeat() override{
		curr_feat_vec.resize(getDistFeatSize());
	}
	const double* getDistFeat() override{ return curr_feat_vec.data(); }

};

_MTF_END_NAMESPACE

#endif