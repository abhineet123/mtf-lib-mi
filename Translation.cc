#include "Translation.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Translation::Translation(int resx, int resy,
TranslationParams *params_in) : ProjectiveBase(resx, resy),
params(params_in){

	printf("\n");
	printf("Initializing Translation state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "translation";
	state_size = 2;

	curr_state.resize(state_size);

	identity_jacobian = true;
}

void Translation::setCorners(const CornersT& corners){
	curr_corners = corners;
	getPtsFromCorners(curr_warp, curr_pts, curr_pts_hm, curr_corners);

	init_corners = curr_corners;
	init_pts = curr_pts;

	curr_warp = Matrix3d::Identity();
	curr_state.fill(0);
}

void Translation::setState(const VectorXd &ssm_state){
	VALIDATE_SSM_STATE(ssm_state);
	curr_state = ssm_state;
	getWarpFromState(curr_warp, curr_state);

	curr_pts = init_pts.colwise() + curr_state;
	curr_corners = init_corners.colwise() + curr_state;
}

void Translation::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	curr_state += state_update;
	curr_warp(0, 2) = curr_state(0);
	curr_warp(1, 2) = curr_state(1);

	curr_pts = curr_pts.colwise() + state_update;
	curr_corners = curr_corners.colwise() + state_update;
}

void Translation::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat.setIdentity();
	warp_mat(0, 2) = ssm_state(0);
	warp_mat(1, 2) = ssm_state(1);
}

void Translation::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_TRANS_WARP(warp_mat);

	state_vec(0) = warp_mat(0, 2);
	state_vec(1) = warp_mat(1, 2);
}

void Translation::invertState(VectorXd& inv_state, const VectorXd& state){
	inv_state = -state;
}

void Translation::updateGradPts(double grad_eps){

	for(int pix_id = 0; pix_id < n_pts; pix_id++){
		grad_pts(0, pix_id) = curr_pts(0, pix_id) + grad_eps;
		grad_pts(1, pix_id) = curr_pts(1, pix_id);

		grad_pts(2, pix_id) = curr_pts(0, pix_id) - grad_eps;
		grad_pts(3, pix_id) = curr_pts(1, pix_id);

		grad_pts(4, pix_id) = curr_pts(0, pix_id);
		grad_pts(5, pix_id) = curr_pts(1, pix_id) + grad_eps;

		grad_pts(6, pix_id) = curr_pts(0, pix_id);
		grad_pts(7, pix_id) = curr_pts(1, pix_id) - grad_eps;
	}
}

void Translation::updateHessPts(double hess_eps){
	double hess_eps2 = 2 * hess_eps;

	for(int pix_id = 0; pix_id < n_pts; pix_id++){

		hess_pts(0, pix_id) = curr_pts(0, pix_id) + hess_eps2;
		hess_pts(1, pix_id) = curr_pts(1, pix_id);

		hess_pts(2, pix_id) = curr_pts(0, pix_id) - hess_eps2;
		hess_pts(3, pix_id) = curr_pts(1, pix_id);

		hess_pts(4, pix_id) = curr_pts(0, pix_id);
		hess_pts(5, pix_id) = curr_pts(1, pix_id) + hess_eps2;

		hess_pts(6, pix_id) = curr_pts(0, pix_id);
		hess_pts(7, pix_id) = curr_pts(1, pix_id) - hess_eps2;

		hess_pts(8, pix_id) = curr_pts(0, pix_id) + hess_eps;
		hess_pts(9, pix_id) = curr_pts(1, pix_id) + hess_eps;

		hess_pts(10, pix_id) = curr_pts(0, pix_id) - hess_eps;
		hess_pts(11, pix_id) = curr_pts(1, pix_id) - hess_eps;

		hess_pts(12, pix_id) = curr_pts(0, pix_id) + hess_eps;
		hess_pts(13, pix_id) = curr_pts(1, pix_id) - hess_eps;

		hess_pts(14, pix_id) = curr_pts(0, pix_id) - hess_eps;
		hess_pts(15, pix_id) = curr_pts(1, pix_id) + hess_eps;
	}
}

void Translation::applyWarpToCorners(Matrix24d &warped_corners, const Matrix24d &orig_corners,
	const VectorXd &state_update){
	warped_corners = orig_corners.colwise() + state_update;
}
void Translation::applyWarpToPts(Matrix2Xd &warped_pts, const Matrix2Xd &orig_pts,
	const VectorXd &state_update){
	warped_pts = orig_pts.colwise() + state_update;
}

void Translation::estimateWarpFromCorners(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);

	Vector2d out_centroid = out_corners.rowwise().mean();
	Vector2d in_centroid = in_corners.rowwise().mean();
	state_update = out_centroid - in_centroid;
}


_MTF_END_NAMESPACE

