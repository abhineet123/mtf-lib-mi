#include "ICLK.h"
#include "miscUtils.h"
#include <time.h>
#include <stdexcept>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
ICLK<AM, SSM >::ICLK(ParamType *iclk_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(iclk_params){

	printf("\n");
	printf("initializing Inverse Compositional Lucas Kanade tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("hess_type: %d\n", params.hess_type);
	printf("sec_ord_hess: %d\n", params.sec_ord_hess);
	printf("debug_mode: %d\n", params.debug_mode);

	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "iclk";
	log_fname = "log/mtf_iclk_log.txt";
	time_fname = "log/mtf_iclk_times.txt";
	frame_id = 0;

	const char *hess_order = params.sec_ord_hess ? "Second" : "First";
	switch(params.hess_type){
	case HessType::InitialSelf:
		printf("Using %s order Initial Self Hessian\n", hess_order);
		break;
	case HessType::CurrentSelf:
		printf("Using %s order Current Self Hessian\n", hess_order);
		break;
	case HessType::Std:
		printf("Using %s order Standard Hessian\n", hess_order);
		break;
	default:
		throw std::invalid_argument("Invalid Hessian type provided");
	}
	init_pix_jacobian.resize(am->getPixCount(), ssm->getStateSize());
	if(params.hess_type == HessType::CurrentSelf){
		curr_pix_jacobian.resize(am->getPixCount(), ssm->getStateSize());
	}
	jacobian.resize(ssm->getStateSize());
	hessian.resize(ssm->getStateSize(), ssm->getStateSize());
	ssm_update.resize(ssm->getStateSize());
	inv_update.resize(ssm->getStateSize());

	if(params.sec_ord_hess){
		if(params.hess_type == HessType::CurrentSelf){
			curr_pix_hessian.resize(ssm->getStateSize()*ssm->getStateSize(), am->getPixCount());
		} else {
			init_pix_hessian.resize(ssm->getStateSize()*ssm->getStateSize(), am->getPixCount());
		}
	}
}

template <class AM, class SSM>
void ICLK<AM, SSM >::initialize(const cv::Mat &corners){
	start_timer();

	am->clearInitStatus();
	ssm->clearInitStatus();

	ssm->initialize(corners);
	am->initializePixVals(ssm->getPts());
	am->initializePixGrad(ssm->getPts());
	am->initialize();
	am->initializeGrad();
	am->initializeHess();

	ssm->cmptWarpedPixJacobian(init_pix_jacobian, am->getInitPixGrad());
	am->cmptInitJacobian(jacobian, init_pix_jacobian);

	if(params.sec_ord_hess){
		am->initializePixHess(ssm->getPts());
		if(params.hess_type != HessType::CurrentSelf){
			ssm->cmptWarpedPixHessian(init_pix_hessian, am->getInitPixHess(), am->getInitPixGrad());
		}
	}
	if(params.hess_type == HessType::InitialSelf){
		if(params.sec_ord_hess){
			am->cmptSelfHessian(hessian, init_pix_jacobian, init_pix_hessian);
		} else{
			am->cmptSelfHessian(hessian, init_pix_jacobian);
		}
	}
	ssm->getCorners(cv_corners);

	end_timer();
	write_interval(time_fname, "w");
}

template <class AM, class SSM>
void ICLK<AM, SSM >::setRegion(const cv::Mat& corners){
	ssm->setCorners(corners);
	// since the above command completely resets the SSM state including its initial points,
	// any quantities that depend on these, like init_pix_jacobian and init_pix_hessian,
	// must be recomputed along with quantities that depend on these in turn.

	ssm->cmptWarpedPixJacobian(init_pix_jacobian, am->getInitPixGrad());
	am->cmptInitJacobian(jacobian, init_pix_jacobian);

	if(params.sec_ord_hess){
		if(params.hess_type != HessType::CurrentSelf){
			ssm->cmptWarpedPixHessian(init_pix_hessian, am->getInitPixHess(), am->getInitPixGrad());
		}
	}
	if(params.hess_type == HessType::InitialSelf){
		if(params.sec_ord_hess){
			am->cmptSelfHessian(hessian, init_pix_jacobian, init_pix_hessian);
		} else{
			am->cmptSelfHessian(hessian, init_pix_jacobian);
		}
	}
	ssm->getCorners(cv_corners);
}

template <class AM, class SSM>
void ICLK<AM, SSM >::update(){
	++frame_id;
	write_frame_id(frame_id);

	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
		init_timer();

		am->updatePixVals(ssm->getPts());
		record_event("am->updatePixVals");

		am->update();
		record_event("am->update");

		am->updateInitGrad();
		record_event("am->updateInitGrad");

		am->cmptInitJacobian(jacobian, init_pix_jacobian);
		record_event("am->cmptInitJacobian");

		switch(params.hess_type){
		case HessType::InitialSelf:
			break;
		case HessType::CurrentSelf:
			am->updatePixGrad(ssm->getPts());
			record_event("am->updatePixGrad");
			ssm->cmptWarpedPixJacobian(curr_pix_jacobian, am->getCurrPixGrad());
			record_event("ssm->cmptWarpedPixJacobian");

			if(params.sec_ord_hess){
				am->updatePixHess(ssm->getPts());
				record_event("am->updatePixHess");
				ssm->cmptWarpedPixHessian(curr_pix_hessian, am->getCurrPixHess(), am->getCurrPixGrad());
				record_event("ssm->cmptWarpedPixHessian");

				am->cmptSelfHessian(hessian, curr_pix_jacobian, curr_pix_hessian);
				record_event("am->cmptSelfHessian (second order)");
			} else{
				am->cmptSelfHessian(hessian, curr_pix_jacobian);
				record_event("am->cmptSelfHessian (first order)");
			}
			break;
		case HessType::Std:
			if(params.sec_ord_hess){
				am->cmptInitHessian(hessian, init_pix_jacobian, init_pix_hessian);
				record_event("am->cmptInitHessian (second order)");
			} else{
				am->cmptInitHessian(hessian, init_pix_jacobian);
				record_event("am->cmptInitHessian (first order)");
			}
			break;
		}

		ssm_update = -hessian.colPivHouseholderQr().solve(jacobian.transpose());
		record_event("ssm_update");

		ssm->invertState(inv_update, ssm_update);
		record_event("ssm->invertState");

		prev_corners = ssm->getCorners();

		ssm->compositionalUpdate(inv_update);
		record_event("ssm->compositionalUpdate");

		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
		record_event("update_norm");

		if(update_norm < params.upd_thresh){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}
		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners);
}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(ICLK);

