#ifndef _PARAMETERS_H
#define _PARAMETERS_H

#define CASCADE_MAX_TRACKERS 10

#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "opencv2/core/core.hpp"

char *fargv[500];

/* default parameters */
int source_id = -1;
int actor_id = 0;
int n_trackers = 0;
char pipeline = 'c';
char img_source = 'j';
int buffer_count = 10;
int buffer_id = 0;

// flags//
int show_xv_window = 0;
int show_cv_window = 1;
int read_objs = 0;
int read_obj_from_gt = 1;
int line_thickness = 2;
int record_frames = 0;
int write_frame_data = 0;
int write_tracking_data = 0;
int write_pts = 1;
int show_warped_img = 0;
int show_template_img = 0;
int write_tracker_states = 0;
int write_objs = 0;
int reinit_from_gt = false;
double err_thresh = 5.0;

char* read_obj_fname = "sel_objs/selected_objects.txt";
char* write_obj_fname = "sel_objs/selected_objects.txt";
char* tracking_data_fname = nullptr;

char *source_name = nullptr;
char *source_fmt = nullptr;
char *source_path = nullptr;
char *root_path = nullptr;
char *actor = nullptr;

// for Xvision trackers
int steps_per_frame = 1;

int patch_size = 0;

char frame_dir[200];
char pts_dir[200];
char states_dir[200];

/* only for pyramidal trackers */
int no_of_levels = 2;
double scale = 0.5;
/* only for color tracker; enabling it causes tracker to hang and crash */
bool color_resample = false;

int search_width = 50;
int search_angle = 0;
int line_width = 4;

/* for xvision grid tracker */
int grid_size_x = 4;
int grid_size_y = -1;
int tracker_type = 't';
int reset_pos = 0;
int reset_template = 0;
int reset_wts = 0;
double sel_reset_thresh = 1.0;

int pause_after_line = 1;
int pause_after_frame = 1;
int show_tracked_pts = 1;
int debug_mode = 0;
int use_constant_slope = 0;
int use_ls = 0;
int update_wts = 0;
double inter_alpha_thresh = 0.10;
double intra_alpha_thresh = 0.05;

// flags//
int adjust_grid = 0;
int adjust_lines = 1;

// for MTF
unsigned int mtf_res = 0;
unsigned int resx = 50;
unsigned int resy = 50;
int init_frame_id = 0;
int end_frame_id = 0;
int max_iters = 10;
double upd_thresh = 0.01;
char* mtf_sm = "esm";
char* mtf_am = "ssd";
char* mtf_ssm = "8";

bool rec_init_err_grad = false;
double norm_pix_min = 0.0;
double norm_pix_max = 1.0;
int res_from_size = 0;
int show_tracking_error = 0;
int hess_type = 0;
bool sec_ord_hess = false;
int jac_type = 0;
char *pix_mapper = nullptr;

int normalized_init = 0;
int update_templ = 0;
double grad_eps = 1e-8;
double hess_eps = 1.0;

// Homograhy
bool hom_direct_samples = true;

// SCV
bool scv_use_bspl = 0;
int scv_n_bins = 256;
double scv_preseed = 0;
bool scv_pou = 1;
bool scv_wt_map = 1;
bool scv_map_grad = 1;
bool scv_affine_mapping = 0;
bool scv_once_per_frame = 0;

// LSCV
int lscv_sub_regions = 3;
int lscv_spacing = 10;
bool lscv_show_subregions = false;

// LKLD
int lkld_n_bins = 8;
double lkld_pre_seed = 0.1;
bool lkld_pou = 1;
int lkld_sub_regions = 0;
int lkld_spacing = 1;

//SSIM
int ssim_pix_proc_type = 0;
double ssim_k1 = 0.01;
double ssim_k2 = 0.03;

// MI & CCRE
int mi_n_bins = 8;
double mi_pre_seed = 10;
bool mi_pou = false;
bool mi_rscv_mapping = false;

// CCRE
bool ccre_symmetrical_grad = false;
int ccre_n_blocks = 0;

// NN and GNN
int nn_max_iters = 10;
int nn_n_samples = 1000;
std::vector<double> nn_ssm_sigma;
double nn_corner_sigma_d = 0.04;
double nn_corner_sigma_t = 0.06;
double nn_pix_sigma = 0;
int nn_n_trees = 6;
int nn_n_checks = 50;
double nn_ssm_sigma_prec = 1.1;
int nn_index_type = 1;
bool nn_additive_update = false;
bool nn_direct_samples = false;

// GNN 
int gnn_k = 250;

//Gradient Descent
double gd_learning_rate = 0.1;

// Particle Filter
int pf_n_particles = 100;
int pf_dyn_model = 1;
bool pf_enable_resampling = true;
bool pf_reset_to_mean = false;
bool pf_mean_of_corners = false;
std::vector<double> pf_ssm_sigma;
double pf_measurement_sigma = 0.1;
double pf_pix_sigma = 0;

// Cascade tracker
int casc_n_trackers = 2;
bool casc_enable_feedback = 1;

// Grid tracker
char* gt_sm = "iclk";
char* gt_am = "ssd";
char* gt_ssm = "2";
int gt_grid_res = 10;
int gt_patch_size = 10;
int gt_patch_size_y = 10;
int gt_estimation_method = 0;
double gt_ransac_reproj_thresh = 10;
bool gt_init_at_each_frame = true;
bool gt_dyn_patch_size = false;
bool gt_show_trackers = false;
bool gt_show_tracker_edges = false;
bool gt_use_tbb = true;

//RKL Tracker
char* rkl_sm = "iclk";
bool rkl_enable_spi = true;
bool rkl_enable_feedback = true;
bool rkl_failure_detection = true;
double rkl_failure_thresh = 15.0;

// MTF Diagnostics
char* diag_am = "ssd";
char* diag_ssm = "2";
int diag_frame_gap = 0;
double diag_range = 0;
double diag_trans_range = 10;
double diag_rot_range = 0.5;
double diag_scale_range = 0.1;
double diag_shear_range = 0.1;
double diag_proj_range = 0.1;
double diag_hom_range = 0.1;
char* diag_gen_norm = "00";// Norm,FeatNorm
char* diag_gen_jac = "000";// Std,ESM,Diff
char* diag_gen_hess = "0000";// Std,ESM,InitSelf,CurrSelf
char* diag_gen_hess2 = "0000";// Std2,ESM2,InitSelf2,CurrSelf2
char* diag_gen_hess_sum = "0000";// Std, Std2, Self, Self2
char* diag_gen_num = "000"; // Jac, Hess, NHess
char* diag_gen_ssm = "0";// ssm params

bool diag_bin = true;
bool diag_inv = true;
bool diag_show_corners = true;
bool diag_show_patches = true;
bool diag_verbose = false;

double diag_grad_diff = 0.1;
int diag_res = 50;
int diag_update = 0;

bool spi_enable = false;
double spi_thresh = 10;

// DSST 
double dsst_padding = 1;
double dsst_sigma = 1.0 / 16;
double dsst_scale_sigma = 1.0 / 4;
double dsst_lambda = 1e-2;
double dsst_learning_rate = 0.025;
int dsst_number_scales = 33;
double dsst_scale_step = 1.02;
int dsst_resize_factor = 4;
int dsst_is_scaling = 1;
int dsst_bin_size = 1;

// CMT
bool cmt_estimate_scale = true;
bool cmt_estimate_rotation = false;
char* cmt_feat_detector = "FAST";
char* cmt_desc_extractor = "BRISK";
double cmt_resize_factor = 0.5;

// TLD
bool tld_tracker_enabled = true;
bool tld_detector_enabled = true;
bool tld_learning_enabled = true;
bool tld_alternating = false;

//RCT
int rct_min_n_rect = 2;
int rct_max_n_rect = 4;
int rct_n_feat = 50;
int rct_rad_outer_pos = 4;
int rct_rad_search_win = 25;
double rct_learning_rate = 0.85;

// HACLK
//std::vector<cv::Mat> conv_corners;

int readParams(char* fname = "params.txt");
void processStringParam(char* &str_out, const char* param);
// convert a string of comma (or other specified character) separated values 
// into a vector of doubles
std::vector<double> atof_arr(char *str, char sep = ',');
void processAgrument(char *arg_name, char *arg_val);
FILE* readTrackerParams(FILE * fid = nullptr, int print_args = 0);
void parseArgumentPairs(char * argv[], int argc,
	int parse_type = 0, int print_args = 0);
void freeParams();


#endif
