#include<iostream>
#include<fstream>
#include<sstream>
#include <stdio.h>
#include <stdlib.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAX_HOVER_TEXT_SIZE 100

////using namespace cv;
using namespace std;

struct obj_struct {
	cv::Point min_point;
	cv::Point max_point;

	double size_x;
	double size_y;
	double pos_x;
	double pos_y;

	cv::Mat corners;

	obj_struct();
	void updateCornerMat();
	void updateCornerPoints();
};


int clicked_point_count;
cv::Point mouse_click_point;
bool point_selected;
bool left_button_clicked;
bool right_button_clicked;
cv::Point mouse_hover_point;
bool mouse_hover_event;
double hover_font_size=0.50;
cv::Scalar hover_color(0, 255, 0);

/* callback function for mouse clicks */
void getClickedPoint(int mouse_event, int x, int y, int flags, void* param);
class CVUtils {
public:
	vector<cv::Scalar> obj_cols;
	int no_of_cols;	
	vector<cv::Mat> ground_truth;

	CVUtils();
	/*allows the user to select a rectangle by clicking on its opposite corners*/
	obj_struct* getObject(cv::Mat&selection_image, string selection_window, int col_id = 0,
		int patch_size = 0);
	vector<obj_struct*> getMultipleObjects(cv::Mat&selection_image, int no_of_objs,
		int patch_size = 0, int write_objs = 0, char* filename = "selected_objects.txt");
	void writeObjectsToFile(vector<obj_struct*> objs, int no_of_objs,
		char* filename = "sel_objs/selected_objects.txt");
	obj_struct* readObjectFromGT(char* source_name, char* source_path,
		int n_frames = 1, int init_frame_id = 0, int debug_mode = 0);	
	vector<obj_struct*> readObjectsFromFile(int no_of_objs,
		char* filename = "sel_objs/selected_objects.txt", int debug_mode = 0);
	cv::Point getMeanPoint(cv::Point *pt_array, int no_of_points);

};

