#include "SSD.h"

_MTF_BEGIN_NAMESPACE

SSD::SSD(ParamType *ssd_params) : 
SSDBase(ssd_params){
	printf("\n");
	printf("Initializing  SSD appearance model with...\n");
	printf("grad_eps: %e\n", grad_eps);
	printf("hess_eps: %e\n", hess_eps);
	name = "ssd";
}

_MTF_END_NAMESPACE

