BASE_CLASSES = AppearanceModel StateSpaceModel SearchMethod TrackerBase mtfMacros mtfRegister
DIAG_BASE_CLASSES = AppearanceModel StateSpaceModel DiagBase mtfMacros mtfRegister

SEARCH_METHODS = FCLK PF NN GNN ESM AESM ICLK FALK IALK FCGD GridTracker ParallelTracker
APPEARANCE_MODELS = ImageBase SSDBase SSD NSSD ZNCC SCV LSCV RSCV LRSCV CCRE LKLD MI SSIM NCC SPSS KLD
STATE_SPACE_MODELS = ProjectiveBase LieHomography CornerHomography Homography Affine Similitude Isometry Transcaling Translation
UTILITIES = histUtils homUtils imgUtils
UTILITIES_H = miscUtils
COMPOSITE_TRACKERS = CascadeTracker RKLT
TOOLS =  parameters datasets inputCV inputBase cvUtils PreProc
DIAG_TOOLS = Diagnostics
MTF_MISC_INCLUDES = GNN/build_graph.h GNN/utility.h

XVISION_TRACKERS = xvSSDAffine xvSSDGrid xvSSDGridLine xvSSDHelper xvSSDMain xvSSDPyramidAffine xvSSDPyramidRotate xvSSDPyramidRT xvSSDPyramidSE2 xvSSDPyramidTrans xvSSDRotate xvSSDRT xvSSDScaling xvSSDSE2 xvSSDTR xvSSDTrans common xvColor xvEdgeTracker
_LEARNING_TRACKERS = DSST/DSST KCF/KCFTracker RCT/CompressiveTracker 
_LEARNING_TRACKERS_SO = cmt opentld cvblobs
TLD_INCLUDES = TLD.h MedianFlowTracker.h DetectorCascade.h DetectionResult.h ForegroundDetector.h VarianceFilter.h IntegralImage.h EnsembleClassifier.h Clustering.h NNClassifier.h NormalizedPatch.h
CMT_INCLUDES = CMT.h common.h logging/log.h Consensus.h Fusion.h Tracker.h gui.h Matcher.h fastcluster/fastcluster.h
DSST_INCLUDES = wrappers.h HOG.h Params.h sse.hpp

SEARCH_OBJS := $(addsuffix .o, ${SEARCH_METHODS})
APPEARANCE_OBJS := $(addsuffix .o, ${APPEARANCE_MODELS})
STATE_SPACE_OBJS := $(addsuffix .o, ${STATE_SPACE_MODELS})	
MTF_UTIL_OBJS := $(addsuffix .o, ${UTILITIES})
DIAG_OBJS := $(addsuffix .o, ${DIAG_TOOLS})

BASE_HEADERS = $(addsuffix .h, ${BASE_CLASSES})
SEARCH_HEADERS = $(addsuffix .h, ${SEARCH_METHODS})
APPEARANCE_HEADERS = $(addsuffix .h, ${APPEARANCE_MODELS})
STATE_SPACE_HEADERS = $(addsuffix .h, ${STATE_SPACE_MODELS})	
MTF_UTIL_HEADERS = $(addsuffix .h, ${UTILITIES})
COMPOSITE_HEADERS = $(addsuffix .h, ${COMPOSITE_TRACKERS})

MTF_UTIL_HEADERS += $(addsuffix .h, ${UTILITIES_H})
TOOL_HEADERS =  $(addprefix Tools/, $(addsuffix .h, ${TOOLS}))
DIAG_BASE_HEADERS = $(addsuffix .h, ${DIAG_BASE_CLASSES})
DIAG_HEADERS = Diagnostics.h

LEARNING_TRACKERS =  $(addprefix ThirdParty/, ${_LEARNING_TRACKERS})
LEARNING_INCLUDES =  $(addprefix ThirdParty/, $(addprefix DSST/, ${DSST_INCLUDES}) $(addprefix CMT/, ${CMT_INCLUDES}) $(addprefix TLD/, ${TLD_INCLUDES}))

LEARNING_TRACKERS_SO =  $(addprefix ${MTF_INSTALL_DIR}/lib, $(addsuffix .so, ${_LEARNING_TRACKERS_SO}))
LEARNING_OBJS = $(addsuffix .o, ${LEARNING_TRACKERS})
LEARNING_HEADERS = $(addsuffix .h, ${LEARNING_TRACKERS})
LEARNING_HEADERS += ${LEARNING_INCLUDES}
XVISION_HEADERS =  $(addprefix ThirdParty/Xvision/, $(addsuffix .h, ${XVISION_TRACKERS}))

#MTF_HEADERS= $(addprefix ${MTF_HEADER_DIR}/, ${BASE_HEADERS} ${SEARCH_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${MTF_UTIL_HEADERS} ${COMPOSITE_HEADERS} ${TOOL_HEADERS} ${DIAG_BASE_HEADERS} ${DIAG_HEADERS} ${LEARNING_HEADERS})  
MTF_HEADERS = $(addprefix ${MTF_HEADER_DIR}/, MTF.h ${BASE_HEADERS} ${SEARCH_HEADERS} ${APPEARANCE_HEADERS} ${STATE_SPACE_HEADERS} ${MTF_UTIL_HEADERS} ${TOOL_HEADERS} ${COMPOSITE_HEADERS} ${DIAG_BASE_HEADERS} ${DIAG_HEADERS} ${MTF_MISC_INCLUDES} ${LEARNING_HEADERS} ${XVISION_HEADERS})  
