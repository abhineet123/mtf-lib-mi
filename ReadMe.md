Implementation of a visual tracking framework that utilizes a modular decomposition of registration based trackers. 
Each tracker within this framework comprises the following 3 modules:
1. Search Method: ESM, IC, IA, FC, FA, NN, PF or RKLT
2. Appearance Model: SSD, ZNCC, SCV, NCC, MI, CCRE or SSIM
3. State Space Model: Homography (8dof), Affine (6 dof), Similitude(4 dof), Isometery (3 dof) or pure Translation (2 dof)

The library is implemented entirely in C++ though a Python interface called `pyMTF` also exists and works seamlessly with [Python Tracking Framework](https://bitbucket.org/abhineet123/python-tracking-framework). 
A Matlab interface is currently under construction too.

Installation:
-------------
Prerequisites:

* [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) should be installed and added to the C/C++ include paths. This can be done, for instance, by running `echo "export C_INCLUDE_PATH=$C_INCLUDE_PATH:/usr/include/eigen3" >> ~/.bashrc` and `echo "export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/include/eigen3" >> ~/.bashrc` assuming that Eigen is installed in `/usr/include/eigen3`
* [OpenCV](http://opencv.org/) should be installed.
* [FLANN library](http://www.cs.ubc.ca/research/flann/) and its dependency [HDF5](https://www.hdfgroup.org/HDF5/release/obtain5.html) should be installed for the NN tracker
* [Intel TBB](https://www.threadingbuildingblocks.org/) / [OpenMP](http://openmp.org/wp/) should be installed if parallelization is to be enabled.
* [ViSP library](https://visp.inria.fr/) should be installed if its [template tracker module](https://visp.inria.fr/template-tracking/) is enabled during compilation (see below).
* [Xvision](https://bitbucket.org/abhineet123/xvision2) should be installed if it is enabled during compilation (see below).

Following make commands are available:

* `make` or `make mtf` : compiles the shared library (.so file)
* `make install` : compiles the shared library if needed and copies it to `/usr/lib`; also copies the headers to `/usr/include/mtf`; this needs sudo permission
	- if sudo permission is not available, then the variables `MTF_INSTALL_DIR` and `MTF_HEADER_DIR` in `mtf_flags.mak` can be modified to install elsewhere
* `make mtfr` compiles the demo file `runMTF.cc` to create an executable that uses this library to track objects
* `make mtfi` : all of the above
* `make mtfp` : compile the Python interface to MTF - this creates a Python module called `pyMTF.so` that serves as a front end for running these trackers from Python.
Usage of this module is fully demonstrated in the `mtfTracker.py` file in our [Python Tracking Framework](https://bitbucket.org/abhineet123/python-tracking-framework)

* `make run` : creates (if needed) and runs the above executable (doesn't compile or install the library)
* Compile time switches for all of the above commands:
	- specifying `xv=1` will enable Xvision trackers and pipeline too (disabled by default).
	- specifying `vp=1` will enable VisP template trackers (disabled by default).
	- specifying `lt=0` will disable the third party open source learning trackers - [DSST](http://www.cvl.isy.liu.se/en/research/objrec/visualtracking/scalvistrack/index.html), [KCF](http://home.isr.uc.pt/~henriques/circulant/), [CMT](http://www.gnebehay.com/cmt/), [TLD](http://www.gnebehay.com/tld/) and [RCT](http://www4.comp.polyu.edu.hk/~cslzhang/CT/CT.htm) - that are also bundled with this library (in `ThirdParty` subfolder).
* `make clean` : removes all the .o files and the .so file created during compilation from the MTF folder
* `make mtfc` : also removes the executable

Setting Parameters:
-------------------

Refer to the ReadMe in the [Tools](https://bitbucket.org/abhineet123/mtf/src/1ef6e2c08ff7c431c51d36ef5f5e7e2536a8b4b2/Tools/?at=master) sub folder for instructions on how to specify parameters for the tracking task.