#include "NSSD.h"

_MTF_BEGIN_NAMESPACE

NSSD::NSSD(ParamType *nssd_params) : 
SSDBase(nssd_params), params(nssd_params){
	printf("\n");
	printf("Initializing  Normalized SSD appearance model with:\n");
	printf("norm_pix_min: %f\n", params.norm_pix_min);
	printf("norm_pix_max: %f\n", params.norm_pix_max);

	name = "normalized ssd";

	pix_norm_mult = (params.norm_pix_max - params.norm_pix_min) / (PIX_MAX - PIX_MIN);
	pix_norm_add = params.norm_pix_min;
}

_MTF_END_NAMESPACE

