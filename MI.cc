#include "MI.h"
#include "histUtils.h"
#include "miscUtils.h"
#include "imgUtils.h"

_MTF_BEGIN_NAMESPACE

MI::ParamType MI::static_params;
int MI::static_n_pix;

MI::MI(ParamType *mi_params) :
AppearanceModel(mi_params), params(mi_params){
	printf("\n");
	printf("Initializing  MI appearance model with:\n");
	printf("n_bins: %d\n", params.n_bins);
	printf("pre_seed: %f\n", params.pre_seed);
	printf("partition_of_unity: %d\n", params.partition_of_unity);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "mi";
	log_fname = "log/mtf_mi_log.txt";
	time_fname = "log/mtf_mi_times.txt";

	if(!params.pix_mapper){
		printf("Pixel mapping is disabled\n");
	}

	double norm_pix_min = 0, norm_pix_max = params.n_bins - 1;
	if(params.partition_of_unity){
		if(params.n_bins < 4){
			throw std::invalid_argument("MI::Too few bins specified to enforce partition of unity constraint");
		}
		norm_pix_min = 1;
		norm_pix_max = params.n_bins - 2;
	}
	printf("norm_pix_min: %f\n", norm_pix_min);
	printf("norm_pix_max: %f\n", norm_pix_max);

	pix_norm_mult = (norm_pix_max - norm_pix_min) / (PIX_MAX - PIX_MIN + 1);
	// extra 1 in the denominator to avoid the extremely rare case when pix val = PIX_MAX 
	// causing problems with the bspline id system of indexing the histogram
	pix_norm_add = norm_pix_min;

	joint_hist_size = params.n_bins * params.n_bins; // size of the flattened MI matrix
	hist_pre_seed = params.n_bins * params.pre_seed;// preseeding the joint histogram by 's' is equivalent to 
	// preseeding individual histograms by s * n_bins

	//hist_mat_pre_seed = hist_pre_seed / static_cast<double>(n_pix); 
	/*since each element of hist is the sum of the corresponding row in hist_mat (that has n_pix elements),
	preseeding each element of hist with 's' is equivalent to preseeding each element of hist_mat with s/n_pix */

	hist_norm_mult = 1.0 / (static_cast<double>(n_pix)+hist_pre_seed * params.n_bins);
	log_hist_norm_mult = log(hist_norm_mult);
	/* denominator of this factor is equal to the sum of all entries in the individual histograms,
	so multiplying hist with this factor will give the normalized hist whose entries sum to 1*/

	_std_bspl_ids.resize(params.n_bins, Eigen::NoChange);
	// _linear_idx(i, j) stores the linear index of element (i,j) of a matrix
	// of dimensions params.n_bins x params.n_bins if it is to be flattened into a vector
	// in row major order; 
	_linear_idx.resize(params.n_bins, params.n_bins);
	for(int i = 0; i < params.n_bins; i++) {
		_std_bspl_ids(i, 0) = max(0, i - 1);
		_std_bspl_ids(i, 1) = min(params.n_bins - 1, i + 2);
		for(int j = 0; j < params.n_bins; j++) {
			_linear_idx(i, j) = i * params.n_bins + j;
		}
	}
	//for functor support
	static_params = mi_params;
	static_n_pix = n_pix;
	feat_size = 5 * static_n_pix;
}

void MI::initializePixVals(const Matrix2Xd& init_pts){
	if(!is_initialized.pix_vals){
		init_pix_vals.resize(n_pix);
		curr_pix_vals.resize(n_pix);
	}
	if(params.pix_mapper){
		params.pix_mapper->initializePixVals(init_pts);
		init_pix_vals = pix_norm_mult*(params.pix_mapper->getInitPixVals()).array() + pix_norm_add;

	} else{
		utils::getPixVals(init_pix_vals, curr_img, init_pts, n_pix,
			img_height, img_width, pix_norm_mult, pix_norm_add);
	}

	if(!is_initialized.pix_vals){
		curr_pix_vals = init_pix_vals;
		is_initialized.pix_vals = true;
	}
}

/**
* Prerequisites :: Computed in:
*		None
* Computes :: Description
*		curr_pix_vals
*/
void MI::updatePixVals(const Matrix2Xd& curr_pts){
	if(params.pix_mapper){
		params.pix_mapper->updatePixVals(curr_pts);
		curr_pix_vals = pix_norm_mult*(params.pix_mapper->getCurrPixVals()).array() + pix_norm_add;
	} else{
		utils::getPixVals(curr_pix_vals, curr_img, curr_pts, n_pix,
			img_height, img_width, pix_norm_mult, pix_norm_add);
	}
}

/**
* initialize
* Prerequisites :: Computed in:
*	init_pix_vals :: initializePixVals
* Computes :: Description
*	_init_bspl_ids :: stores the indices of the first and last bins contributed to by each pixel in the initial template
*	init_hist :: histogram of initial template
*	init_hist_mat :: stores the contribution of each pixel to each histogram bin
*	init_hist_log :: entry wise logarithm of init_hist
*	joint_hist :: joint histogram of the initial template wrt itself
*	joint_hist_log :: entry wise logarithm of joint_hist
*	init_hist_grad :: gradient of the initial histogram w.r.t. pixel values
*/
void MI::initialize(){
	if(!is_initialized.similarity){
		init_hist.resize(params.n_bins);
		init_hist_mat.resize(params.n_bins, n_pix);
		joint_hist.resize(params.n_bins, params.n_bins);
		init_hist_log.resize(params.n_bins);
		joint_hist_log.resize(params.n_bins, params.n_bins);

		curr_hist.resize(params.n_bins);
		curr_hist_mat.resize(params.n_bins, n_pix);
		joint_hist.resize(params.n_bins, params.n_bins);
		curr_hist_log.resize(params.n_bins);
		joint_hist_log.resize(params.n_bins, params.n_bins);

		_init_bspl_ids.resize(n_pix, Eigen::NoChange);
		_curr_bspl_ids.resize(n_pix, Eigen::NoChange);

		// since histogram and its gradients are computed simultaneously, both must be resized here too
		init_hist_grad.resize(params.n_bins, n_pix);
		curr_hist_grad.resize(params.n_bins, n_pix);
	}

	//! initial histogram and its gradient
	init_hist.fill(hist_pre_seed);
	init_hist_mat.setZero();
	init_hist_grad.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++) {
		_init_bspl_ids.row(pix_id) = _std_bspl_ids.row(static_cast<int>(init_pix_vals(pix_id)));
		double curr_diff = _init_bspl_ids(pix_id, 0) - init_pix_vals(pix_id);
		for(int id1 = _init_bspl_ids(pix_id, 0); id1 <= _init_bspl_ids(pix_id, 1); id1++) {
			utils::bSpl3WithGrad(init_hist_mat(id1, pix_id), init_hist_grad(id1, pix_id), curr_diff);
			init_hist_grad(id1, pix_id) *= -hist_norm_mult;
			init_hist(id1) += init_hist_mat(id1, pix_id);
			// since the ids of all bins affected by a pixel are sequential, repeated computation
			// of curr_diff can be avoided by simply incrementing it by 1 which is (hopefully) faster
			++curr_diff;
		}

	}
	/* normalize the histograms and compute their log*/
	init_hist *= hist_norm_mult;
	init_hist_log = init_hist.array().log();

	//utils::printMatrix(joint_hist, "joint_hist");
	//utils::printMatrix(init_hist, "init_hist");
	//utils::printMatrix(joint_hist - joint_hist.transpose(), "joint_hist diff");

	if(!is_initialized.similarity){
		joint_hist.fill(params.pre_seed);
		for(int pix_id = 0; pix_id < n_pix; pix_id++) {
			for(int id1 = _init_bspl_ids(pix_id, 0); id1 <= _init_bspl_ids(pix_id, 1); id1++) {
				for(int id2 = _init_bspl_ids(pix_id, 0); id2 <= _init_bspl_ids(pix_id, 1); id2++) {
					joint_hist(id1, id2) += init_hist_mat(id1, pix_id) * init_hist_mat(id2, pix_id);
				}
			}
		}
		joint_hist *= hist_norm_mult;
		joint_hist_log = joint_hist.array().log();

		similarity = 0;
		for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
			for(int init_id = 0; init_id < params.n_bins; init_id++){
				similarity += joint_hist(curr_id, init_id) * (joint_hist_log(curr_id, init_id) -
					init_hist_log(curr_id) - init_hist_log(init_id));
			}
		}
		max_similarity = similarity;

		_curr_bspl_ids = _init_bspl_ids;
		curr_hist = init_hist;
		curr_hist_mat = init_hist_mat;
		curr_hist_log = init_hist_log;
		curr_hist_grad = init_hist_grad;

		is_initialized.similarity = true;
	}
}

/**
* initializeGrad
* Prerequisites :: Computed in:
*	init_hist_mat, _init_bspl_ids, init_hist_grad,
init_hist_log:: initialize
* Computes :: Description
*	init_grad_factor :: 1 + log(joint_hist/init_hist) - useful for computing the MI gradient
*	init_grad: gradient of MI w.r.t. initial pixel values
*/

void MI::initializeGrad(){
	if(!is_initialized.grad){
		init_joint_hist_grad.resize(joint_hist_size, n_pix);
		curr_joint_hist_grad.resize(joint_hist_size, n_pix);

		init_grad_factor.resize(params.n_bins, params.n_bins);
		curr_grad_factor.resize(params.n_bins, params.n_bins);

		curr_grad.resize(n_pix);
		init_grad.resize(n_pix);

		for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
			for(int init_id = 0; init_id < params.n_bins; init_id++){
				init_grad_factor(curr_id, init_id) = 1 + joint_hist_log(curr_id, init_id) - init_hist_log(curr_id);
			}
		}
		// gradient of the initial joint histogram and MI  w.r.t. initial pixel values; the latter does not need to be normalized 
		// since init_hist_grad has already been normalized and the computed differential will thus be implicitly normalized
		init_joint_hist_grad.setZero();
		init_grad.setZero();
		for(int pix_id = 0; pix_id < n_pix; pix_id++) {
			for(int curr_id = _init_bspl_ids(pix_id, 0); curr_id <= _init_bspl_ids(pix_id, 1); curr_id++) {
				for(int init_id = _init_bspl_ids(pix_id, 0); init_id <= _init_bspl_ids(pix_id, 1); init_id++) {
					int joint_id = _linear_idx(curr_id, init_id);
					init_joint_hist_grad(joint_id, pix_id) = init_hist_grad(curr_id, pix_id) * init_hist_mat(init_id, pix_id);
					init_grad(pix_id) += init_joint_hist_grad(joint_id, pix_id) * init_grad_factor(curr_id, init_id);
				}
			}
		}
		curr_grad_factor = init_grad_factor;
		curr_joint_hist_grad = init_joint_hist_grad;
		curr_grad = init_grad;
		is_initialized.grad = true;
	}
}
/**
* update
* Prerequisites :: Computed in:
*	curr_pix_vals :: updatePixVals
*	_init_bspl_ids, init_hist_mat :: initialize
* Computes :: Description
*	_curr_bspl_ids :: stores the indices of the first and last bins contributed to by each pixel in the initial template
*	curr_hist :: histogram of cuurent pixel values
*	curr_hist_mat :: stores the contribution of each pixel to each histogram bin
*	joint_hist :: joint histogram between the current and initial pixel values
*	curr_hist_log :: entry wise logarithm of curr_hist
*	joint_hist_log :: entry wise logarithm of joint_hist
*	curr_hist_grad :: gradient of the current histogram w.r.t. current pixel values
*	curr_joint_hist_grad :: gradient of the current joint histogram w.r.t. current pixel values
*	similarity :: MI between the current and initial pixel values (optional)
*/
void MI::update(bool prereq_only){

	// compute both the histogram and its differential simultaneously
	// to take advantage of the many common computations involved
	curr_hist.fill(hist_pre_seed);
	joint_hist.fill(params.pre_seed);
	curr_hist_mat.setZero();
	curr_hist_grad.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++) {
		_curr_bspl_ids.row(pix_id) = _std_bspl_ids.row(static_cast<int>(curr_pix_vals(pix_id)));
		double curr_diff = _curr_bspl_ids(pix_id, 0) - curr_pix_vals(pix_id);
		for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
			utils::bSpl3WithGrad(curr_hist_mat(curr_id, pix_id), curr_hist_grad(curr_id, pix_id), curr_diff);
			++curr_diff;
			curr_hist_grad(curr_id, pix_id) *= -hist_norm_mult;
			curr_hist(curr_id) += curr_hist_mat(curr_id, pix_id);
			for(int init_id = _init_bspl_ids(pix_id, 0); init_id <= _init_bspl_ids(pix_id, 1); init_id++) {
				joint_hist(curr_id, init_id) += curr_hist_mat(curr_id, pix_id) * init_hist_mat(init_id, pix_id);
			}
		}
	}

	// normalize the histograms and compute their log
	curr_hist *= hist_norm_mult;
	joint_hist *= hist_norm_mult;
	curr_hist_log = curr_hist.array().log();
	joint_hist_log = joint_hist.array().log();

	if(prereq_only){ return; }

	similarity = 0;
	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			similarity += joint_hist(curr_id, init_id) * (joint_hist_log(curr_id, init_id) - curr_hist_log(curr_id) - init_hist_log(init_id));
		}
	}
}
/**
* updateInitGrad
* Prerequisites :: Computed in:
*	init_hist_grad, _init_bspl_ids, init_hist_log :: initialize
*	curr_hist_mat, _curr_bspl_ids, joint_hist_log :: update
* Computes :: Description
*	init_joint_hist_grad :: gradient of the current joint histogram w.r.t. initial pixel values
*	init_grad_factor :: 1 + log(joint_hist/init_hist) - useful for computing the MI gradient too
*	init_grad: gradient of current MI w.r.t. initial pixel values
*/
void MI::updateInitGrad(){
	for(int init_id = 0; init_id < params.n_bins; init_id++){
		for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
			init_grad_factor(init_id, curr_id) = 1 + joint_hist_log(curr_id, init_id) - init_hist_log(init_id);
		}
	}
	// differential of the current joint histogram w.r.t. initial pixel values; this does not need to be normalized 
	// since init_hist_grad has already been normalized and the computed differential will thus be implicitly normalized
	init_joint_hist_grad.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++) {
		init_grad(pix_id) = 0;
		for(int init_id = _init_bspl_ids(pix_id, 0); init_id <= _init_bspl_ids(pix_id, 1); init_id++) {
			for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
				int joint_id = _linear_idx(init_id, curr_id);
				init_joint_hist_grad(joint_id, pix_id) = init_hist_grad(init_id, pix_id) * curr_hist_mat(curr_id, pix_id);
				init_grad(pix_id) += init_joint_hist_grad(joint_id, pix_id) * init_grad_factor(init_id, curr_id);
			}
		}
	}
}
/**
* updateCurrGrad
* Prerequisites :: Computed in:
*	joint_hist_log, curr_hist_log, curr_joint_hist_grad :: update
* Computes :: Description
*	curr_grad_factor :: 1 + log(joint_hist/curr_hist) - useful for computing the MI gradient too
*	curr_grad: gradient of current MI w.r.t. current pixel values
*/
void MI::updateCurrGrad(){
	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			curr_grad_factor(curr_id, init_id) = 1 + joint_hist_log(curr_id, init_id) - curr_hist_log(curr_id);
		}
	}
	curr_joint_hist_grad.setZero();
	for(int pix = 0; pix < n_pix; pix++){
		curr_grad(pix) = 0;
		for(int curr_id = _curr_bspl_ids(pix, 0); curr_id <= _curr_bspl_ids(pix, 1); curr_id++) {
			for(int init_id = _init_bspl_ids(pix, 0); init_id <= _init_bspl_ids(pix, 1); init_id++) {
				int joint_id = _linear_idx(curr_id, init_id);
				curr_joint_hist_grad(joint_id, pix) = curr_hist_grad(curr_id, pix) * init_hist_mat(init_id, pix);
				curr_grad(pix) += curr_joint_hist_grad(joint_id, pix) * curr_grad_factor(curr_id, init_id);
			}
		}
	}
}
void MI::initializeHess(){
	if(!is_initialized.hess){
		init_hist_hess.resize(params.n_bins, n_pix);
		curr_hist_hess.resize(params.n_bins, n_pix);

		self_joint_hist.resize(params.n_bins, params.n_bins);
		self_joint_hist_log.resize(params.n_bins, params.n_bins);
		self_grad_factor.resize(params.n_bins, params.n_bins);
	}

	utils::getBSplHistHess(init_hist_hess, init_pix_vals, _init_bspl_ids, n_pix, hist_norm_mult);

	if(!is_initialized.hess){
		curr_hist_hess = init_hist_hess;
		is_initialized.hess = true;
	}
}
void  MI::cmptInitHessian(MatrixXd &hessian, const MatrixXd &init_pix_jacobian){
	int ssm_state_size = init_pix_jacobian.cols();
	assert(hessian.rows() == ssm_state_size && hessian.cols() == ssm_state_size);
	assert(init_pix_jacobian.rows() == n_pix);

	MatrixXd joint_hist_jacobian(joint_hist_size, ssm_state_size);
	hessian.setZero();
	joint_hist_jacobian.setZero();

	//utils::printMatrix(_curr_bspl_ids - _init_bspl_ids, "_init_bspl_ids diff", "%d");
	//utils::printMatrix(joint_hist_log - self_joint_hist_log, "joint_hist_log diff", "%e");
	//utils::printMatrix(init_hist_log - curr_hist_log, "init_hist_log diff", "%e");
	//utils::printMatrix(init_hist_hess - curr_hist_hess, "init_hist_hess diff", "%e");
	//utils::printMatrix(_curr_bspl_ids, "_curr_bspl_ids", "%d");
	//utils::printMatrix(_init_bspl_ids, "_init_bspl_ids", "%d");

	//utils::printMatrix(hessian, "init: hessian start", "%e");

	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double hist_hess_term = 0;
		for(int init_id = _init_bspl_ids(pix_id, 0); init_id <= _init_bspl_ids(pix_id, 1); init_id++) {
			double inner_term = 0;
			for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
				int joint_id = _linear_idx(curr_id, init_id);
				//init_joint_hist_grad(joint_id, pix_id) = init_hist_grad(curr_id, pix_id) * init_hist_mat(init_id, pix_id);
				//init_grad_factor(curr_id, init_id) = 1 + joint_hist_log(curr_id, init_id) - init_hist_log(curr_id);				
				joint_hist_jacobian.row(joint_id) += init_joint_hist_grad(_linear_idx(init_id, curr_id), pix_id)*init_pix_jacobian.row(pix_id);
				inner_term += curr_hist_mat(curr_id, pix_id) * init_grad_factor(init_id, curr_id);

				//joint_hist_jacobian.row(joint_id) += init_hist_grad(init_id, pix_id)*curr_hist_mat(curr_id, pix_id)*init_pix_jacobian.row(pix_id);
				//inner_term += curr_hist_mat(curr_id, pix_id) * (1 + joint_hist_log(curr_id, init_id) - init_hist_log(init_id));
			}
			hist_hess_term += init_hist_hess(init_id, pix_id)*inner_term;
		}
		//utils::printScalar(hist_hess_term, "init: hist_hess_term", "%20.16f");

		hessian += hist_hess_term * init_pix_jacobian.row(pix_id).transpose() * init_pix_jacobian.row(pix_id);
		//utils::printMatrix(init_pix_jacobian.row(pix_id).transpose() * init_pix_jacobian.row(pix_id), "init: init_pix_jacobian term", "%20.16f");
		//utils::printMatrix(hessian, "init: hessian", "%20.16f");

	}
	//utils::printMatrix(init_pix_jacobian, "init: init_pix_jacobian", "%e");
	//utils::printMatrix(joint_hist, "init: joint_hist", "%e");
	//utils::printMatrix(joint_hist_jacobian, "init: joint_hist_jacobian", "%e");
	//utils::printMatrix(hessian, "init: hessian 1", "%e");

	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			int joint_id = _linear_idx(curr_id, init_id);
			double hist_factor = (1.0 / joint_hist(curr_id, init_id)) - (1.0 / init_hist(init_id));
			hessian += joint_hist_jacobian.row(joint_id).transpose() * joint_hist_jacobian.row(joint_id) * hist_factor;
		}
	}
}
void MI::cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian){
	int ssm_state_size = curr_pix_jacobian.cols();

	assert(self_hessian.rows() == ssm_state_size && self_hessian.cols() == ssm_state_size);
	assert(curr_pix_jacobian.rows() == n_pix);

	cmptSelfHist();

	MatrixXd self_grad_factor2(params.n_bins, params.n_bins);
	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			self_grad_factor2(curr_id, init_id) = 1 + self_joint_hist_log(curr_id, init_id) - curr_hist_log(init_id);
		}
	}
	MatrixXd joint_hist_jacobian2(joint_hist_size, ssm_state_size);
	MatrixXd self_hessian2(ssm_state_size, ssm_state_size);
	self_hessian2.setZero();
	joint_hist_jacobian2.setZero();
	//utils::printMatrix(self_hessian2, "self: self_hessian2 start", "%e");
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double hist_hess_term2 = 0;
		for(int init_id = _curr_bspl_ids(pix_id, 0); init_id <= _curr_bspl_ids(pix_id, 1); init_id++) {
			double inner_term2 = 0;
			for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
				int joint_id = _linear_idx(curr_id, init_id);
				joint_hist_jacobian2.row(joint_id) += curr_hist_grad(init_id, pix_id)*curr_hist_mat(curr_id, pix_id)*curr_pix_jacobian.row(pix_id);
				inner_term2 += curr_hist_mat(curr_id, pix_id) * self_grad_factor2(curr_id, init_id);
			}
			hist_hess_term2 += curr_hist_hess(init_id, pix_id)*inner_term2;
		}
		self_hessian2 += hist_hess_term2 * curr_pix_jacobian.row(pix_id).transpose() * curr_pix_jacobian.row(pix_id);
		//utils::printScalar(hist_hess_term2, "self: hist_hess_term2", "%20.16f");
		//utils::printMatrix(curr_pix_jacobian.row(pix_id).transpose() * curr_pix_jacobian.row(pix_id), "self: curr_pix_jacobian term", "%20.16f");
		//utils::printMatrix(self_hessian2, "self: self_hessian2", "%20.16f");

	}
	//utils::printMatrix(curr_pix_jacobian, "self: curr_pix_jacobian", "%e");
	//utils::printMatrix(self_joint_hist, "self: self_joint_hist", "%e");
	//utils::printMatrix(joint_hist_jacobian2, "self: joint_hist_jacobian2", "%e");
	//utils::printMatrix(self_hessian2, "self: self_hessian2 1", "%e");

	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			int joint_id = _linear_idx(curr_id, init_id);
			double hist_factor = (1.0 / self_joint_hist(curr_id, init_id)) - (1.0 / curr_hist(init_id));
			self_hessian2 += joint_hist_jacobian2.row(joint_id).transpose() * joint_hist_jacobian2.row(joint_id) * hist_factor;
		}
	}


	MatrixXd joint_hist_jacobian(joint_hist_size, ssm_state_size);
	self_hessian.setZero();
	joint_hist_jacobian.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double curr_diff = _curr_bspl_ids(pix_id, 0) - curr_pix_vals(pix_id);
		double hist_hess_term = 0;
		for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
			curr_hist_hess(curr_id, pix_id) = hist_norm_mult*utils::bSpl3Hess(curr_diff);
			++curr_diff;
			double inner_term = 0;
			for(int init_id = _curr_bspl_ids(pix_id, 0); init_id <= _curr_bspl_ids(pix_id, 1); init_id++) {
				int idx = _linear_idx(curr_id, init_id);
				joint_hist_jacobian.row(idx) += curr_hist_grad(curr_id, pix_id)*curr_hist_mat(init_id, pix_id)*curr_pix_jacobian.row(pix_id);
				inner_term += curr_hist_mat(init_id, pix_id) * self_grad_factor(curr_id, init_id);
			}
			hist_hess_term += curr_hist_hess(curr_id, pix_id)*inner_term;
		}
		self_hessian += hist_hess_term * curr_pix_jacobian.row(pix_id).transpose() * curr_pix_jacobian.row(pix_id);
	}
	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			int joint_id = _linear_idx(curr_id, init_id);
			double hist_factor = (1.0 / self_joint_hist(curr_id, init_id)) - (1.0 / curr_hist(curr_id));
			self_hessian += joint_hist_jacobian.row(joint_id).transpose() * joint_hist_jacobian.row(joint_id) * hist_factor;
		}
	}
	//utils::printMatrix(self_grad_factor - self_grad_factor2, "self_grad_factor diff", "%e");
	//utils::printMatrix(joint_hist_jacobian - joint_hist_jacobian2, "joint_hist_jacobian diff", "%e");
	//utils::printMatrix(self_hessian - self_hessian2, "self_hessian diff", "%e");
}
/**
cmptCurrHessian
* Prerequisites :: Computed in:
*	curr_joint_hist_grad, curr_hist_grad :: updateCurrErrVec if fast joint histogram is enabled; updateCurrErrVecGrad otherwise
*	joint_hist, curr_hist :: updateCurrErrVec
* Computes::Description
*	hessian::first order Hessian of current MI w.r.t. current pixel values
*/
void  MI::cmptCurrHessian(MatrixXd &hessian, const MatrixXd &curr_pix_jacobian){
	int ssm_state_size = curr_pix_jacobian.cols();
	assert(hessian.rows() == ssm_state_size && hessian.cols() == ssm_state_size);
	assert(curr_pix_jacobian.rows() == n_pix);

	// jacobians of the joint and current histograms w.r.t. SSM parameters
	//MatrixXd joint_hist_jacobian = curr_joint_hist_grad * curr_pix_jacobian;

	MatrixXd joint_hist_jacobian(joint_hist_size, ssm_state_size);
	hessian.setZero();
	joint_hist_jacobian.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double curr_diff = _curr_bspl_ids(pix_id, 0) - curr_pix_vals(pix_id);
		double hist_hess_term = 0;
		for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
			curr_hist_hess(curr_id, pix_id) = hist_norm_mult*utils::bSpl3Hess(curr_diff);
			++curr_diff;
			double inner_term = 0;
			for(int init_id = _init_bspl_ids(pix_id, 0); init_id <= _init_bspl_ids(pix_id, 1); init_id++) {
				int idx = _linear_idx(curr_id, init_id);
				joint_hist_jacobian.row(idx) += curr_joint_hist_grad(idx, pix_id)*curr_pix_jacobian.row(pix_id);
				inner_term += init_hist_mat(init_id, pix_id) * curr_grad_factor(curr_id, init_id);
			}
			hist_hess_term += curr_hist_hess(curr_id, pix_id)*inner_term;
		}
		hessian += hist_hess_term * curr_pix_jacobian.row(pix_id).transpose() * curr_pix_jacobian.row(pix_id);
	}
	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			int idx = _linear_idx(curr_id, init_id);
			double hist_factor = (1.0 / joint_hist(curr_id, init_id)) - (1.0 / curr_hist(curr_id));
			hessian += joint_hist_jacobian.row(idx).transpose() * joint_hist_jacobian.row(idx) * hist_factor;
		}
	}
}
// compute self histogram and its derivarives
void MI::cmptSelfHist(){
	//! current joint histogram computed wrt itself
	self_joint_hist.fill(params.pre_seed);
	for(int pix = 0; pix < n_pix; pix++) {
		for(int id1 = _curr_bspl_ids(pix, 0); id1 <= _curr_bspl_ids(pix, 1); id1++) {
			for(int id2 = _curr_bspl_ids(pix, 0); id2 <= _curr_bspl_ids(pix, 1); id2++) {
				self_joint_hist(id1, id2) += curr_hist_mat(id1, pix) * curr_hist_mat(id2, pix);
			}
		}
	}
	/* normalize the self joint histogram and compute its log*/
	self_joint_hist *= hist_norm_mult;
	self_joint_hist_log = self_joint_hist.array().log();

	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
		for(int init_id = 0; init_id < params.n_bins; init_id++){
			self_grad_factor(curr_id, init_id) = 1 + self_joint_hist_log(curr_id, init_id) - curr_hist_log(curr_id);
		}
	}
}
void MI::cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
	const MatrixXd &init_pix_hessian){

	int ssm_state_size = init_pix_jacobian.cols();

	assert(init_hessian.rows() == ssm_state_size && init_hessian.cols() == ssm_state_size);
	assert(init_pix_jacobian.rows() == n_pix);
	assert(init_pix_hessian.rows() == ssm_state_size*ssm_state_size);

	cmptInitHessian(init_hessian, init_pix_jacobian);

	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		init_hessian += init_grad(pix_id) * MatrixXdM((double*)init_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size);
	}
}
/**
* Prerequisites :: Computed in:
*		[joint_hist_log, curr_hist_log] :: updateCurrGrad
* Computes :: Description:
*		curr_hessian :: hessian of the error norm w.r.t. current SSM parameters
*/
void MI::cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
	const MatrixXd &curr_pix_hessian){
	int ssm_state_size = curr_pix_jacobian.cols();

	assert(curr_hessian.rows() == ssm_state_size && curr_hessian.cols() == ssm_state_size);
	assert(curr_pix_jacobian.rows() == n_pix);
	assert(curr_pix_hessian.rows() == ssm_state_size*ssm_state_size);

	// get first order hessian
	cmptCurrHessian(curr_hessian, curr_pix_jacobian);

	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		curr_hessian += curr_grad(pix_id) * MatrixXdM((double*)curr_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size);
	}
}

void MI::cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
	const MatrixXd &curr_pix_hessian){
	int ssm_state_size = curr_pix_jacobian.cols();

	assert(self_hessian.rows() == ssm_state_size && self_hessian.cols() == ssm_state_size);
	assert(curr_pix_jacobian.rows() == n_pix);
	assert(curr_pix_hessian.rows() == ssm_state_size*ssm_state_size);

	cmptSelfHist();
	MatrixXd joint_hist_jacobian(joint_hist_size, ssm_state_size);
	self_hessian.setZero();
	joint_hist_jacobian.setZero();
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double curr_diff = _curr_bspl_ids(pix_id, 0) - curr_pix_vals(pix_id);
		double hist_hess_term = 0, hist_grad_term = 0;
		for(int r = _curr_bspl_ids(pix_id, 0); r <= _curr_bspl_ids(pix_id, 1); r++) {
			curr_hist_hess(r, pix_id) = hist_norm_mult*utils::bSpl3Hess(curr_diff++);
			double inner_term = 0;
			for(int t = _curr_bspl_ids(pix_id, 0); t <= _curr_bspl_ids(pix_id, 1); t++) {
				int idx = _linear_idx(r, t);
				joint_hist_jacobian.row(idx) += curr_hist_grad(r, pix_id)*curr_hist_mat(t, pix_id)*curr_pix_jacobian.row(pix_id);
				inner_term += curr_hist_mat(t, pix_id) * self_grad_factor(r, t);
			}
			hist_hess_term += curr_hist_hess(r, pix_id)*inner_term;
			hist_grad_term += curr_hist_grad(r, pix_id)*inner_term;
		}
		self_hessian += hist_hess_term * curr_pix_jacobian.row(pix_id).transpose() * curr_pix_jacobian.row(pix_id)
			+ hist_grad_term * MatrixXdM((double*)curr_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size);
	}
	for(int r = 0; r < params.n_bins; r++){
		for(int t = 0; t < params.n_bins; t++){
			int idx = _linear_idx(r, t);
			double hist_factor = (1.0 / self_joint_hist(r, t)) - (1.0 / curr_hist(r));
			self_hessian += joint_hist_jacobian.row(idx).transpose() * joint_hist_jacobian.row(idx) * hist_factor;
		}
	}
}

//-----------------------------------functor support-----------------------------------//
MI::MI() : AppearanceModel(){

	printf("Initializing MI functor with: \n");
	printf("static n_pix: %d\n", static_n_pix);
	printf("static n_bins: %d\n", static_params.n_bins);
	hist_pre_seed = static_params.n_bins * static_params.pre_seed;
	hist_norm_mult = 1.0 / (static_cast<double>(static_n_pix)+hist_pre_seed*static_params.n_bins);
	log_hist_norm_mult = log(hist_norm_mult);
	feat_size = 5 * static_n_pix;
	double norm_pix_min = 0, norm_pix_max = params.n_bins - 1;
	if(static_params.partition_of_unity){
		if(static_params.n_bins < 4){
			throw std::invalid_argument("MI::Too few bins specified to enforce partition of unity constraint");
		}
		norm_pix_min = 1;
		norm_pix_max = static_params.n_bins - 2;
	}
	printf("norm_pix_min: %f\n", norm_pix_min);
	printf("norm_pix_max: %f\n", norm_pix_max);

	pix_norm_mult = (norm_pix_max - norm_pix_min) / (PIX_MAX - PIX_MIN + 1);
	pix_norm_add = norm_pix_min;

	_std_bspl_ids.resize(static_params.n_bins, Eigen::NoChange);
	for(int i = 0; i < static_params.n_bins; i++) {
		_std_bspl_ids(i, 0) = max(0, i - 1);
		_std_bspl_ids(i, 1) = min(static_params.n_bins - 1, i + 2);
	}
}

void MI::updateDistFeat(double* feat_addr){
	MatrixXdMr hist_mat((double*)feat_addr, 5, static_n_pix);
	for(size_t pix = 0; pix < static_n_pix; pix++) {
		int pix_val_floor = static_cast<int>(curr_pix_vals(pix));
		double pix_diff = _std_bspl_ids(pix_val_floor, 0) - curr_pix_vals(pix);
		hist_mat(0, pix) = pix_val_floor;
		hist_mat(1, pix) = utils::bSpl3(pix_diff);
		hist_mat(2, pix) = utils::bSpl3(++pix_diff);
		hist_mat(3, pix) = utils::bSpl3(++pix_diff);
		hist_mat(4, pix) = utils::bSpl3(++pix_diff);
	}
}

double MI::operator()(const double* hist1_mat_addr, const double* hist2_mat_addr,
	size_t hist_mat_size, double worst_dist) const{

	//printf("hist_mat_size: %ld\n", hist_mat_size);
	//printf("feat_size: %d\n", feat_size);

	assert(hist_mat_size == feat_size);

	VectorXd hist1(static_params.n_bins);
	VectorXd hist2(static_params.n_bins);
	MatrixXd joint_hist(static_params.n_bins, static_params.n_bins);

	hist1.fill(hist_pre_seed);
	hist2.fill(hist_pre_seed);
	joint_hist.fill(static_params.pre_seed);

	MatrixXdMr hist1_mat((double*)hist1_mat_addr, 5, static_n_pix);
	MatrixXdMr hist2_mat((double*)hist2_mat_addr, 5, static_n_pix);

	//utils::printMatrixToFile(hist1_mat, "hist1_mat", "log/mi_log.txt");
	//utils::printMatrixToFile(hist2_mat, "hist2_mat", "log/mi_log.txt");


	for(size_t pix_id = 0; pix_id < static_n_pix; pix_id++) {
		int pix1_floor = static_cast<int>(hist1_mat(0, pix_id));
		int pix2_floor = static_cast<int>(hist2_mat(0, pix_id));

		//printf("pix1_floor: %d\n", pix1_floor);
		//if(pix2_floor >= static_params.n_bins){
		//	utils::printMatrixToFile(hist2_mat, "hist2_mat", "log/mi_log.txt");
		//	printf("pix2_floor: %d\n", pix2_floor);
		//}


		int bspl_id11 = _std_bspl_ids(pix1_floor, 0);
		int bspl_id12 = bspl_id11 + 1, bspl_id13 = bspl_id11 + 2, bspl_id14 = bspl_id11 + 3;
		int bspl_id21 = _std_bspl_ids(pix2_floor, 0);
		int bspl_id22 = bspl_id21 + 1, bspl_id23 = bspl_id21 + 2, bspl_id24 = bspl_id21 + 3;

		hist1(bspl_id11) += hist1_mat(1, pix_id);
		hist1(bspl_id12) += hist1_mat(2, pix_id);
		hist1(bspl_id13) += hist1_mat(3, pix_id);
		hist1(bspl_id14) += hist1_mat(4, pix_id);

		hist2(bspl_id21) += hist2_mat(1, pix_id);
		hist2(bspl_id22) += hist2_mat(2, pix_id);
		hist2(bspl_id23) += hist2_mat(3, pix_id);
		hist2(bspl_id24) += hist2_mat(4, pix_id);

		joint_hist(bspl_id11, bspl_id21) += hist1_mat(1, pix_id) * hist2_mat(1, pix_id);
		joint_hist(bspl_id12, bspl_id21) += hist1_mat(2, pix_id) * hist2_mat(1, pix_id);
		joint_hist(bspl_id13, bspl_id21) += hist1_mat(3, pix_id) * hist2_mat(1, pix_id);
		joint_hist(bspl_id14, bspl_id21) += hist1_mat(4, pix_id) * hist2_mat(1, pix_id);

		joint_hist(bspl_id11, bspl_id22) += hist1_mat(1, pix_id) * hist2_mat(2, pix_id);
		joint_hist(bspl_id12, bspl_id22) += hist1_mat(2, pix_id) * hist2_mat(2, pix_id);
		joint_hist(bspl_id13, bspl_id22) += hist1_mat(3, pix_id) * hist2_mat(2, pix_id);
		joint_hist(bspl_id14, bspl_id22) += hist1_mat(4, pix_id) * hist2_mat(2, pix_id);

		joint_hist(bspl_id11, bspl_id23) += hist1_mat(1, pix_id) * hist2_mat(3, pix_id);
		joint_hist(bspl_id12, bspl_id23) += hist1_mat(2, pix_id) * hist2_mat(3, pix_id);
		joint_hist(bspl_id13, bspl_id23) += hist1_mat(3, pix_id) * hist2_mat(3, pix_id);
		joint_hist(bspl_id14, bspl_id23) += hist1_mat(4, pix_id) * hist2_mat(3, pix_id);

		joint_hist(bspl_id11, bspl_id24) += hist1_mat(1, pix_id) * hist2_mat(4, pix_id);
		joint_hist(bspl_id12, bspl_id24) += hist1_mat(2, pix_id) * hist2_mat(4, pix_id);
		joint_hist(bspl_id13, bspl_id24) += hist1_mat(3, pix_id) * hist2_mat(4, pix_id);
		joint_hist(bspl_id14, bspl_id24) += hist1_mat(4, pix_id) * hist2_mat(4, pix_id);
	}
	//curr_hist *= hist_norm_mult;
	//init_hist *= hist_norm_mult;
	//joint_hist *= hist_norm_mult;

	//utils::printMatrixToFile(joint_hist, "joint_hist", "log/mi_log.txt");
	//utils::printMatrixToFile(hist1, "hist1", "log/mi_log.txt");
	//utils::printMatrixToFile(hist2, "hist2", "log/mi_log.txt");

	ResultType result = 0;
	for(int id1 = 0; id1 < static_params.n_bins; id1++){
		for(int id2 = 0; id2 < static_params.n_bins; id2++){
			result -= joint_hist(id1, id2) * (log(joint_hist(id1, id2) / (hist1(id1) * hist2(id2))) - log_hist_norm_mult);
		}
	}
	//result *= hist_norm_mult;
	return result;
}

//void  MI::cmptInitHessian(MatrixXd &hessian, const MatrixXd &init_pix_jacobian){
//	int ssm_state_size = init_pix_jacobian.cols();
//	assert(hessian.rows() == ssm_state_size && hessian.cols() == ssm_state_size);
//
//
//	clock_t start_time, end_time;
//
//	start_time = clock();
//	MatrixXd joint_hist_jacobian = init_joint_hist_grad*init_pix_jacobian;
//	MatrixXd joint_hist_hessian(ssm_state_size, ssm_state_size);
//	hessian.setZero();
//	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
//		for(int init_id = 0; init_id < params.n_bins; init_id++){
//			joint_hist_hessian.setZero();
//			for(int pix_id = 0; pix_id < n_pix; pix_id++){
//				joint_hist_hessian += curr_hist_mat(curr_id, pix_id) * init_hist_hess(init_id, pix_id) * init_pix_jacobian.row(pix_id).transpose() * init_pix_jacobian.row(pix_id);
//			}
//			int joint_id = _linear_idx(curr_id, init_id);
//			hessian += joint_hist_jacobian.row(joint_id).transpose() * joint_hist_jacobian.row(joint_id) *  ((1.0 / joint_hist(curr_id, init_id)) - (1.0 / init_hist(init_id)))
//				+ joint_hist_hessian*init_grad_factor(curr_id, init_id);
//		}
//	}
//	end_time = clock();
//	printf("Naive time: %ld\n", end_time - start_time);
//
//	MatrixXd naive_hessian = hessian;
//
//	start_time = clock();
//	hessian.setZero();
//	joint_hist_jacobian.setZero();
//	for(int pix_id = 0; pix_id < n_pix; pix_id++){
//		double hist_hess_term = 0;
//		for(int init_id = _init_bspl_ids(pix_id, 0); init_id <= _init_bspl_ids(pix_id, 1); init_id++) {
//			double inner_term = 0;
//			for(int curr_id = _curr_bspl_ids(pix_id, 0); curr_id <= _curr_bspl_ids(pix_id, 1); curr_id++) {
//				int joint_id = _linear_idx(curr_id, init_id);
//				joint_hist_jacobian.row(joint_id) += init_joint_hist_grad(joint_id, pix_id)*init_pix_jacobian.row(pix_id);
//				inner_term += curr_hist_mat(curr_id, pix_id) * init_grad_factor(curr_id, init_id);
//			}
//			hist_hess_term += init_hist_hess(init_id, pix_id)*inner_term;
//		}
//		hessian += hist_hess_term * init_pix_jacobian.row(pix_id).transpose() * init_pix_jacobian.row(pix_id);
//	}
//	for(int curr_id = 0; curr_id < params.n_bins; curr_id++){
//		for(int init_id = 0; init_id < params.n_bins; init_id++){
//			int joint_id = _linear_idx(curr_id, init_id);
//			double hist_factor = (1.0 / joint_hist(curr_id, init_id)) - (1.0 / init_hist(init_id));
//			hessian += joint_hist_jacobian.row(joint_id).transpose() * joint_hist_jacobian.row(joint_id) * hist_factor;
//		}
//	}
//	end_time = clock();
//	printf("Efficient time: %ld\n", end_time-start_time);
//	utils::printMatrix(naive_hessian - hessian, "hessian_diff");
//}


//void cmptSelfHessianSlow(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian){
//	MatrixXd joint_hist_jacobian(joint_hist_size, ssm_state_size);
//	MatrixXd joint_hist_hess(ssm_state_size*ssm_state_size, joint_hist_size);
//	MatrixXd pix_jacobian_sqr(ssm_state_size, ssm_state_size);
//
//	joint_hist_jacobian.setZero();
//	joint_hist_hess.setZero();
//	joint_hist.fill(params.pre_seed);
//
//	for(int pix_id = 0; pix_id < n_pix; pix_id++){
//		double curr_diff = _curr_bspl_ids(pix_id, 0) - curr_pix_vals(pix_id);
//		pix_jacobian_sqr.noalias() = curr_pix_jacobian.row(pix_id).transpose() * curr_pix_jacobian.row(pix_id);
//		for(int r = _curr_bspl_ids(pix_id, 0); r <= _curr_bspl_ids(pix_id, 1); r++) {
//			curr_hist_hess(r, pix_id) = hist_norm_mult*utils::bSpl3Hess(curr_diff++);
//			for(int t = _curr_bspl_ids(pix_id, 0); t <= _curr_bspl_ids(pix_id, 1); t++) {
//				joint_hist(r, t) += curr_hist_mat(r, pix_id) * curr_hist_mat(t, pix_id);
//				int idx = _linear_idx(r, t);
//				joint_hist_jacobian.row(idx) += 
//					curr_hist_grad(r, pix_id)*curr_hist_mat(t, pix_id)*curr_pix_jacobian.row(pix_id);
//				Map<MatrixXd>(joint_hist_hess.col(idx).data(), ssm_state_size, ssm_state_size) +=
//					curr_hist_hess(r, pix_id)*curr_hist_mat(t, pix_id)*pix_jacobian_sqr;
//			}
//		}
//	}
//	joint_hist *= hist_norm_mult;
//	joint_hist_log = joint_hist.array().log();
//
//	self_hessian.setZero();
//	for(int r = 0; r < params.n_bins; r++){
//		for(int t = 0; t < params.n_bins; t++){
//			int idx = _linear_idx(r, t);
//			double factor_1 = (1.0 / joint_hist(r, t)) - (1.0 / curr_hist(r));
//			double factor_2 = 1 + joint_hist_log(r, t) - curr_hist_log(r);
//			self_hessian += joint_hist_jacobian.row(idx).transpose() * joint_hist_jacobian.row(idx) * factor_1
//				+ Map<MatrixXd>(joint_hist_hess.col(idx).data(), ssm_state_size, ssm_state_size)*factor_2;
//		}
//	}
//
//}

_MTF_END_NAMESPACE

