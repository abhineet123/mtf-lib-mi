#ifndef ESM_H
#define ESM_H

#include "SearchMethod.h"

#define ESM_MAX_ITERS 10
#define ESM_UPD_THRESH 0.01
#define ESM_JAC_TYPE 0
#define ESM_HESS_TYPE 0
#define ESM_SEC_ORD_HESS false
#define ESM_ENABLE_SPI false
#define ESM_SPI_THRESH 10
#define ESM_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct ESMParams{

	enum class JacType{ Original, DiffOfJacs };
	enum class HessType {
		Original, SumOfStd, SumOfSelf,
		InitialSelf, CurrentSelf, Std
	};

	int max_iters; //! maximum iterations of the ESM algorithm to run for each frame
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations

	JacType jac_type;
	HessType hess_type;
	bool sec_ord_hess;

	bool enable_spi;
	double spi_thresh;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	// value constructor
	ESMParams(int _max_iters, double _upd_thresh, 
		JacType _jac_type, HessType _hess_type, bool _sec_ord_hess,
		bool _enable_spi, double _spi_thresh,
		bool _debug_mode){
		max_iters = _max_iters;
		upd_thresh = _upd_thresh;
		jac_type = _jac_type;
		hess_type = _hess_type;
		sec_ord_hess = _sec_ord_hess;
		enable_spi = _enable_spi;
		spi_thresh = _spi_thresh;
		debug_mode = _debug_mode;
	}
	// default and copy constructor
	ESMParams(ESMParams *params = nullptr) :
		max_iters(ESM_MAX_ITERS), upd_thresh(ESM_UPD_THRESH),
		jac_type(static_cast<JacType>(ESM_JAC_TYPE)),
		hess_type(static_cast<HessType>(ESM_HESS_TYPE)),
		sec_ord_hess(ESM_SEC_ORD_HESS),
		enable_spi(ESM_ENABLE_SPI), spi_thresh(ESM_SPI_THRESH),
		debug_mode(ESM_DEBUG_MODE){
		if(params){
			max_iters = params->max_iters;
			upd_thresh = params->upd_thresh;
			jac_type = params->jac_type;
			hess_type = params->hess_type;
			sec_ord_hess = params->sec_ord_hess;
			enable_spi = params->enable_spi;
			spi_thresh = params->spi_thresh;
			debug_mode = params->debug_mode;
		}
	}
};

template<class AM, class SSM>
class ESM : public SearchMethod < AM, SSM > {

protected:
	init_profiling();
	char *time_fname;
	char *log_fname;

	void initializeSPIMask();
	void updateSPIMask();
	void showSPIMask();

public:
	typedef ESMParams ParamType;
	ParamType params;

	typedef typename ParamType::JacType JacType;
	typedef typename ParamType::HessType HessType;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::cv_corners;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	int frame_id;
	VectorXc pix_mask2;
	VectorXb pix_mask;
	VectorXd rel_pix_diff;
	cv::Mat pix_mask_img;
	double max_pix_diff;
	char* spi_win_name;

	Matrix24d prev_corners;

	//! N x S jacobians of the pixel values w.r.t the SSM state vector where N = resx * resy
	//! is the no. of pixels in the object patch
	MatrixXd init_pix_jacobian, curr_pix_jacobian, mean_pix_jacobian;
	MatrixXd init_pix_hessian, curr_pix_hessian, mean_pix_hessian;

	VectorXd ssm_update;

	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
	RowVectorXd jacobian;
	//! S x S Hessian of the AM error norm w.r.t. SSM state vector
	MatrixXd hessian, init_self_hessian;

	ESM(ParamType *nesm_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);

	void initialize(const cv::Mat &corners) override;
	void update() override;
	void setRegion(const cv::Mat& corners) override;

	void cmptJacobian();
	void cmptHessian(); 

	// functions re implemented by AESM to get the additive variant
	virtual void initializePixJacobian();
	virtual void updatePixJacobian();
	virtual void initializePixHessian();
	virtual void updatePixHessian();
	virtual void updateSSM();


};
_MTF_END_NAMESPACE

#endif

