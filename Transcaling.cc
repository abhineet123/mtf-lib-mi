#include "Transcaling.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Transcaling::Transcaling(int resx, int resy, TranscalingParams *params_in) : 
ProjectiveBase(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Transcaling state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "Transcaling";
	state_size = 3;
	curr_state.resize(state_size);
}

void Transcaling::setState(const VectorXd &ssm_state){
	VALIDATE_SSM_STATE(ssm_state);
	curr_state = ssm_state;
	getWarpFromState(curr_warp, curr_state);
	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Transcaling::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;

	getStateFromWarp(curr_state, curr_warp);

	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Transcaling::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat = Matrix3d::Identity();
	warp_mat(0, 0) = warp_mat(1, 1) = 1 + ssm_state(2);
	warp_mat(0, 2) = ssm_state(0);
	warp_mat(1, 2) = ssm_state(1);
}

void Transcaling::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& sim_mat){
	VALIDATE_SSM_STATE(state_vec);
	VALIDATE_TRS_WARP(sim_mat);

	state_vec(0) = sim_mat(0, 2);
	state_vec(1) = sim_mat(1, 2);
	state_vec(2) = sim_mat(0, 0) - 1;
}

void Transcaling::cmptInitPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Ix*x + Iy*y;
	}
}

void Transcaling::cmptApproxPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double s_plus_1_inv = 1.0 / (curr_state(2) + 1);
	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix * s_plus_1_inv;
		jacobian_prod(i, 1) = Iy * s_plus_1_inv;
		jacobian_prod(i, 2) = (Ix*x + Iy*y) * s_plus_1_inv;
	}
}

void Transcaling::getInitPixGrad(Matrix2Xd &ssm_grad, int pix_id) {
	double x = init_pts(0, pix_id);
	double y = init_pts(1, pix_id);
	ssm_grad <<
		1, 0, x,
		0, 1, y;
}

void Transcaling::cmptInitPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
	const PixGradT &pix_grad){
	VALIDATE_SSM_HESSIAN(pix_hess_ssm, pix_hess_coord, pix_grad);

	Matrix23d ssm_grad;
	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		double x = init_pts(0, pt_id);
		double y = init_pts(1, pt_id);
		ssm_grad <<
			1, 0, x,
			0, 1, y;
		Map<Matrix3d> curr_pix_hess_ssm((double*)pix_hess_ssm.col(pt_id).data());
		curr_pix_hess_ssm = ssm_grad.transpose()*Map<Matrix2d>((double*)pix_hess_coord.col(pt_id).data())*ssm_grad;
	}
}

void Transcaling::estimateWarpFromCorners(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);

	Matrix3d warp_update_mat = utils::computeTranscalingDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

void Transcaling::updateGradPts(double grad_eps){
	double scaled_eps = curr_warp(0, 0) * grad_eps;
	for(int pix_id = 0; pix_id < n_pts; pix_id++){
		grad_pts(0, pix_id) = curr_pts(0, pix_id) + scaled_eps;
		grad_pts(1, pix_id) = curr_pts(1, pix_id);

		grad_pts(2, pix_id) = curr_pts(0, pix_id) - scaled_eps;
		grad_pts(3, pix_id) = curr_pts(1, pix_id);

		grad_pts(4, pix_id) = curr_pts(0, pix_id);
		grad_pts(5, pix_id) = curr_pts(1, pix_id) + scaled_eps;

		grad_pts(6, pix_id) = curr_pts(0, pix_id);
		grad_pts(7, pix_id) = curr_pts(1, pix_id) - scaled_eps;
	}
}


void Transcaling::updateHessPts(double hess_eps){
	double scaled_eps = curr_warp(0, 0) * hess_eps;
	double scaled_eps2 = 2 * scaled_eps;

	for(int pix_id = 0; pix_id < n_pts; pix_id++){

		hess_pts(0, pix_id) = curr_pts(0, pix_id) + scaled_eps2;
		hess_pts(1, pix_id) = curr_pts(1, pix_id);

		hess_pts(2, pix_id) = curr_pts(0, pix_id) - scaled_eps2;
		hess_pts(3, pix_id) = curr_pts(1, pix_id);

		hess_pts(4, pix_id) = curr_pts(0, pix_id);
		hess_pts(5, pix_id) = curr_pts(1, pix_id) + scaled_eps2;

		hess_pts(6, pix_id) = curr_pts(0, pix_id);
		hess_pts(7, pix_id) = curr_pts(1, pix_id) - scaled_eps2;

		hess_pts(8, pix_id) = curr_pts(0, pix_id) + scaled_eps;
		hess_pts(9, pix_id) = curr_pts(1, pix_id) + scaled_eps;

		hess_pts(10, pix_id) = curr_pts(0, pix_id) - scaled_eps;
		hess_pts(11, pix_id) = curr_pts(1, pix_id) - scaled_eps;

		hess_pts(12, pix_id) = curr_pts(0, pix_id) + scaled_eps;
		hess_pts(13, pix_id) = curr_pts(1, pix_id) - scaled_eps;

		hess_pts(14, pix_id) = curr_pts(0, pix_id) - scaled_eps;
		hess_pts(15, pix_id) = curr_pts(1, pix_id) + scaled_eps;
	}
}

void Transcaling::applyWarpToCorners(Matrix24d &warped_corners, const Matrix24d &orig_corners,
	const VectorXd &state_update){
	warped_corners.noalias() = (orig_corners * (state_update(2) + 1)).colwise() + state_update.head<2>();
}
void Transcaling::applyWarpToPts(Matrix2Xd &warped_pts, const Matrix2Xd &orig_pts,
	const VectorXd &state_update){
	warped_pts.noalias() = (orig_pts * (state_update(2) + 1)).colwise() + state_update.head<2>();
}

_MTF_END_NAMESPACE

